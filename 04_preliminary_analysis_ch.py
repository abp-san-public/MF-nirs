#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

LAG_SECONDS = 5
SYNCH_NAME = f'DTW_{LAG_SECONDS}'

TARGET = 'copresence_average'
AGE = 'age'

#%%
data = []
for CH in np.arange(1, 21):
    
    data_ch = []
    for COND in ['SEP', 'TOG']:
        for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_curr['sound'] = np.repeat(SOUND, len(data_curr))
            data_curr['cond'] = np.repeat(COND, len(data_curr))
            data_curr['ch'] = np.repeat(CH, len(data_curr))
            data.append(data_curr)
            data_ch.append(data_curr)

    data_ch = pd.concat(data_ch, axis=0)
    
    print(f'CH{CH}')
    lm_model = ols(f'{TARGET} ~ {AGE} + {AGE}*C(cond) + C(cond)',
                    data=data_ch).fit()

    table = sm.stats.anova_lm(lm_model, typ=2) # Type 2 ANOVA DataFrame
    print(table.loc[:,['PR(>F)']])
    print()
    
#%%
data = pd.concat(data, axis=0)

#%%
lm_model = ols(f'{TARGET} ~ {AGE} + {AGE}*C(cond) + {AGE}*C(sound) + {AGE}*C(ch)',
               data=data).fit()

table = sm.stats.anova_lm(lm_model, typ=2) # Type 2 ANOVA DataFrame
print(table.loc[:,['df', 'PR(>F)']])
print()