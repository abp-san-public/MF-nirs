#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import physynch as ps
import numpy as np
import os
# import matplotlib.pyplot as plt

#%% custom functions
def normalize(x):
    return((x-np.mean(x))/np.std(x))
    
def load_datapoint(sound, dyad, parent, rep, ch, demean=True, rescale=False):
    data = pd.read_csv(f'{DATADIR}/{sound}/{dyad}/{parent}_{rep}.csv')
    data_ch = data[ch].values
    
    data_ch_surr, _, _ = ps.surrogate_IAAFT(data_ch)
    data_ch_surr = np.convolve(data_ch_surr, np.ones(10)/10, 'same')
  
    if demean:
        data_ch = data_ch - np.mean(data_ch)
        data_ch_surr = data_ch_surr - np.mean(data_ch_surr)
    
    if rescale:
        data_ch = data_ch/np.std(data_ch)
        data_ch_surr = data_ch_surr/np.std(data_ch_surr)
        
    return(data_ch, data_ch_surr)

#%% set params
COND = 'TOG'
FSAMP = 7.81
#signal params
DEMEAN = True
RESCALE = True

#synch params
LAG_SECONDS = 5
LAG = int(LAG_SECONDS*FSAMP)

NORMALIZE = True
DATADIR = f'{BASEPATH}/data/signals/{COND}'

#%% for all sounds
for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
    # plt.figure()
    # plt.subplot(5,4,1)
    # plt.suptitle(SOUND)
    
    dyads = os.listdir(f'{DATADIR}/{SOUND}')

    OUTDIR = f'{BASEPATH}/data/synch_CC_{LAG_SECONDS}_average_signals/{COND}/{SOUND}'
    os.makedirs(OUTDIR, exist_ok=True)
    
    #% for all channels
    for CH in np.arange(1, 21):
        
        data = {}
        used_dyads = []
        for DYAD in dyads:
            
            #for all repetitions
            ch_m_rep = []
            ch_surr_m_rep = []
            
            ch_f_rep = []
            ch_surr_f_rep = []
            
            for REP in [1,2,3]: 
                #% load all data for the rep
                ch_m, ch_surr_m = load_datapoint(SOUND, DYAD, 'M', REP, f'CH{CH}', DEMEAN, RESCALE)
                ch_f, ch_surr_f = load_datapoint(SOUND, DYAD, 'F', REP, f'CH{CH}', DEMEAN, RESCALE)
                
                ch_m_rep.append(ch_m)
                ch_surr_m_rep.append(ch_surr_m)
                
                ch_f_rep.append(ch_f)
                ch_surr_f_rep.append(ch_surr_f)
            
            max_len_m = min([len(x) for x in ch_m_rep])
            max_len_f = min([len(x) for x in ch_f_rep])
            max_len = min([max_len_m, max_len_f])

            ch_m_rep = [x[:max_len] for x in ch_m_rep]
            ch_surr_m_rep = [x[:max_len] for x in ch_m_rep]
            
            ch_f_rep = [x[:max_len] for x in ch_f_rep]
            ch_surr_f_rep = [x[:max_len] for x in ch_f_rep]
            
            ch_m_rep = np.array(ch_m_rep)
            ch_surr_m_rep = np.array(ch_surr_m_rep)
            
            ch_f_rep = np.array(ch_f_rep)
            ch_surr_f_rep = np.array(ch_surr_f_rep)
            
            if ~np.isnan(ch_m_rep).any() and ~np.isnan(ch_f_rep).any():
                data_dyad = {}
                data_dyad['M'] = ch_m_rep.mean(axis=0)
                data_dyad['M_surr'] = ch_surr_m_rep.mean(axis=0)
                
                data_dyad['F'] = ch_f_rep.mean(axis=0)
                data_dyad['F_surr'] = ch_surr_f_rep.mean(axis=0)
                
                data[DYAD] = data_dyad
                used_dyads.append(DYAD)
        
        #% regroup
        group_1 = []
        group_2 = []
        for DYAD in used_dyads:
            mother = data[DYAD]['M']
            mother_surr = data[DYAD]['M_surr']
            group_1.append([mother, mother_surr])
        
            father = data[DYAD]['F']
            father_surr = data[DYAD]['F_surr']
            group_2.append([father, father_surr])
        
        #% compute synch
        #DTW
        # copresence, surr_dyads, _, _, surr_signals = ps.compute_distances_golland(group_1, group_2, 'dtw', 
                                                                                  # {'method':'Euclidean','step':'asymmetric', 'wtype':'sakoechiba', 'openend':True, 'openbegin':True, 'wsize':LAG, 'normalize': NORMALIZE})
        
        #CC
        copresence, surr_dyads, _, _, surr_signals = ps.compute_distances_golland(group_1, group_2, 'cc', 
                                                                                  {'lag':LAG, 'normalize':NORMALIZE})
        
        copresence_average_pd = pd.DataFrame(copresence, index = used_dyads, columns=['copresence'])
        copresence_average_pd.to_csv(f'{OUTDIR}/CH{CH}_copresence.csv')
        
        surr_dyads_average_pd = pd.DataFrame(surr_dyads, columns=['surr_dyads'])
        surr_dyads_average_pd.to_csv(f'{OUTDIR}/CH{CH}_surrdyads.csv')
        
        surr_signals_average_pd = pd.DataFrame(surr_signals, columns=['surr_signals'])
        surr_signals_average_pd.to_csv(f'{OUTDIR}/CH{CH}_surrsignals.csv')
        
        # plt.subplot(5,4, CH)
        # plt.title(f'{SOUND} - CH{CH} [{COND}]')
        # plt.hist(copresence_average, color = 'b', histtype='step', density=True)
        # plt.hist(surr_dyads_average, color = 'g', histtype='step', density=True)
        # plt.hist(surr_signals_average, color = 'r', histtype='step', density=True)
