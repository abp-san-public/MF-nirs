#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
from physynch.physynch import compute_distance, DTWDistance, CrossCorrDistance
import numpy as np
import os

np.random.seed(1234)

#% custom functions
def load_datapoint(cond, sound, dyad, parent, rep, ch, demean=False, rescale=False, t0=False):
    data = pd.read_csv(f'{BASEPATH}/data/signals/{cond}/{sound}/{dyad}/{parent}/{ch}/{rep}.csv')
    data_ch = data[ch].values
    
    if demean:
        data_ch = data_ch - np.mean(data_ch)
    
    if rescale:
        data_ch = data_ch/np.std(data_ch)
    
    if t0:
        data_ch = data_ch - data_ch[0]
        
    return(data_ch)

#%% set params
#signal params
FSAMP = 7.81
DEMEAN = False
RESCALE = False
T0 = True

#synch params
LAG_SECONDS = 2
LAG = int(LAG_SECONDS*FSAMP)
NORMALIZE = True
DISTANCE = CrossCorrDistance(LAG, NORMALIZE)
# DISTANCE = DTWDistance(wsize=LAG, normalize=NORMALIZE)
SYNCH_NAME = f'CC_{LAG_SECONDS}_t0'

# load ages
child_age = pd.read_csv(f'{BASEPATH}/data/metadata/child_age.csv', index_col = 0)

#%%
for COND in ['SEP', 'TOG']:
    
    for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
    
        dyads = os.listdir(f'{BASEPATH}/data/signals/{COND}/{SOUND}')
        
        #%
        for CH  in np.arange(1, 21):
        
            #% load all data
            data = {}
            used_dyads = []
            
            for DYAD in dyads:
                
                DATADIR_M = f'{BASEPATH}/data/signals/{COND}/{SOUND}/{DYAD}/M/CH{CH}'
                DATADIR_F = f'{BASEPATH}/data/signals/{COND}/{SOUND}/{DYAD}/F/CH{CH}'
            
                if os.path.exists(DATADIR_F) and os.path.exists(DATADIR_M):
                    n_reps_M = len(os.listdir(DATADIR_M))
                    n_reps_F = len(os.listdir(DATADIR_F))
                
                    if n_reps_M == 3 and n_reps_F == 3:
                        data_dyad = {}
                        
                        for REP in [1,2,3]:
                            ch_m = load_datapoint(COND, SOUND, DYAD, 'M', REP, f'CH{CH}', DEMEAN, RESCALE, T0)
                            ch_f = load_datapoint(COND, SOUND, DYAD, 'F', REP, f'CH{CH}', DEMEAN, RESCALE, T0)
                        
                            data_rep = {'M': ch_m, 'F': ch_f}
                            
                            data_dyad[REP] = data_rep
                        
                        used_dyads.append(DYAD)
                        data[DYAD] = data_dyad
                        
                    # else:
                        # print(COND, SOUND, DYAD, CH, 'not enough reps')
                        
                # else:
                    # print(COND, SOUND, DYAD, CH, 'no channel')
            
            used_dyads = np.array(used_dyads)    
            true_dyads = used_dyads.copy()
            
            #create surr_dyads
            done = False
            while not done:
                np.random.shuffle(used_dyads)
                if ~(used_dyads == true_dyads).any():
                    done=True
            
            surr_dyads = used_dyads.copy()
            
            #compute synchronies for each rep
            copresence = []
            stimulus = []
            
            for REP in [1,2,3]:
                #% copresence
                copresence_rep = []
                for DYAD in true_dyads:
                    signal_M = data[DYAD][REP]['M']
                    signal_F = data[DYAD][REP]['F']
                    
                    copresence_rep.append(compute_distance(signal_M, signal_F, DISTANCE)) #true dyad, true signals
                    
                #% stimulus
                stimulus_rep = []
                for DYAD_M, DYAD_F in zip(true_dyads, surr_dyads):
                    signal_M = data[DYAD_M][REP]['M']
                    signal_F = data[DYAD_F][REP]['F']
                    
                    stimulus_rep.append(compute_distance(signal_M, signal_F, DISTANCE)) #true dyad, true signals
                
                copresence.append(copresence_rep)            
                stimulus.append(stimulus_rep)
            #%
            copresence = pd.DataFrame(np.array(copresence).transpose(), columns = ['copresence_1', 'copresence_2', 'copresence_3'], index = true_dyads)
            stimulus = pd.DataFrame(np.array(stimulus).transpose(), columns = ['stimulus_1', 'stimulus_2', 'stimulus_3'], index = true_dyads)
            
            #% compute average
            copresence['copresence_average'] = copresence.mean(axis=1)
            stimulus['stimulus_average'] = stimulus.mean(axis=1)
            
            #% compue age
            copresence['age'] = child_age.loc[copresence.index,:].values
            stimulus['surr_age'] = child_age.loc[surr_dyads,:].values
            
            #% merge copresence and stimulus
            synchrony = pd.concat([copresence, stimulus], axis = 1)
            
            #% compute average age
            average_age = synchrony.loc[:, ['age', 'surr_age']].mean(axis=1)
            synchrony['average_age'] = average_age
            
            synchrony['surr_dyads'] = surr_dyads
            os.makedirs(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}', exist_ok=True)
            synchrony.to_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv')
    
'''
ERRORS:
SEP FEM_CRY SSC36 1 no channel
SEP FEM_CRY SSC36 7 no channel
SEP FEM_CRY SSC36 8 no channel
SEP FEM_CRY SSC26 10 no channel
SEP FEM_CRY SSC37 11 no channel
SEP FEM_CRY SSC21 12 no channel
SEP FEM_CRY SSC37 13 no channel
SEP FEM_CRY SSC36 15 no channel
SEP FEM_CRY SSC36 16 no channel
SEP FEM_CRY SSC33 17 no channel
SEP FEM_CRY SSC19 19 no channel
SEP FEM_LAUGH SSC36 1 no channel
SEP FEM_LAUGH SSC31 1 no channel
SEP FEM_LAUGH SSC31 2 no channel
SEP FEM_LAUGH SSC31 3 no channel
SEP FEM_LAUGH SSC31 4 no channel
SEP FEM_LAUGH SSC31 5 no channel
SEP FEM_LAUGH SSC31 6 no channel
SEP FEM_LAUGH SSC36 7 no channel
SEP FEM_LAUGH SSC31 7 no channel
SEP FEM_LAUGH SSC36 8 no channel
SEP FEM_LAUGH SSC31 8 no channel
SEP FEM_LAUGH SSC31 9 no channel
SEP FEM_LAUGH SSC26 10 no channel
SEP FEM_LAUGH SSC31 10 no channel
SEP FEM_LAUGH SSC37 11 no channel
SEP FEM_LAUGH SSC31 11 no channel
SEP FEM_LAUGH SSC21 12 no channel
SEP FEM_LAUGH SSC31 12 no channel
SEP FEM_LAUGH SSC37 13 no channel
SEP FEM_LAUGH SSC31 13 no channel
SEP FEM_LAUGH SSC31 14 no channel
SEP FEM_LAUGH SSC36 15 no channel
SEP FEM_LAUGH SSC31 15 no channel
SEP FEM_LAUGH SSC36 16 no channel
SEP FEM_LAUGH SSC31 16 no channel
SEP FEM_LAUGH SSC33 17 no channel
SEP FEM_LAUGH SSC31 17 no channel
SEP FEM_LAUGH SSC31 18 no channel
SEP FEM_LAUGH SSC31 19 no channel
SEP FEM_LAUGH SSC19 19 no channel
SEP FEM_LAUGH SSC31 20 no channel
SEP INF_CRY_HI SSC36 1 no channel
SEP INF_CRY_HI SSC31 1 no channel
SEP INF_CRY_HI SSC31 2 no channel
SEP INF_CRY_HI SSC31 3 no channel
SEP INF_CRY_HI SSC31 4 no channel
SEP INF_CRY_HI SSC31 5 no channel
SEP INF_CRY_HI SSC31 6 no channel
SEP INF_CRY_HI SSC36 7 no channel
SEP INF_CRY_HI SSC31 7 no channel
SEP INF_CRY_HI SSC36 8 no channel
SEP INF_CRY_HI SSC31 8 no channel
SEP INF_CRY_HI SSC31 9 no channel
SEP INF_CRY_HI SSC26 10 no channel
SEP INF_CRY_HI SSC31 10 no channel
SEP INF_CRY_HI SSC37 11 no channel
SEP INF_CRY_HI SSC31 11 no channel
SEP INF_CRY_HI SSC21 12 no channel
SEP INF_CRY_HI SSC31 12 no channel
SEP INF_CRY_HI SSC37 13 no channel
SEP INF_CRY_HI SSC31 13 no channel
SEP INF_CRY_HI SSC31 14 no channel
SEP INF_CRY_HI SSC36 15 no channel
SEP INF_CRY_HI SSC31 15 no channel
SEP INF_CRY_HI SSC36 16 no channel
SEP INF_CRY_HI SSC31 16 no channel
SEP INF_CRY_HI SSC33 17 no channel
SEP INF_CRY_HI SSC31 17 no channel
SEP INF_CRY_HI SSC31 18 no channel
SEP INF_CRY_HI SSC31 19 no channel
SEP INF_CRY_HI SSC19 19 no channel
SEP INF_CRY_HI SSC31 20 no channel
SEP INF_CRY_LO SSC36 1 no channel
SEP INF_CRY_LO SSC36 7 no channel
SEP INF_CRY_LO SSC36 8 no channel
SEP INF_CRY_LO SSC26 10 no channel
SEP INF_CRY_LO SSC37 11 no channel
SEP INF_CRY_LO SSC21 12 no channel
SEP INF_CRY_LO SSC37 13 no channel
SEP INF_CRY_LO SSC36 15 no channel
SEP INF_CRY_LO SSC36 16 no channel
SEP INF_CRY_LO SSC33 17 no channel
SEP INF_CRY_LO SSC19 19 no channel
SEP INF_LAUGH SSC36 1 no channel
SEP INF_LAUGH SSC31 1 no channel
SEP INF_LAUGH SSC31 2 no channel
SEP INF_LAUGH SSC31 3 no channel
SEP INF_LAUGH SSC31 4 no channel
SEP INF_LAUGH SSC31 5 no channel
SEP INF_LAUGH SSC31 6 no channel
SEP INF_LAUGH SSC36 7 no channel
SEP INF_LAUGH SSC31 7 no channel
SEP INF_LAUGH SSC36 8 no channel
SEP INF_LAUGH SSC31 8 no channel
SEP INF_LAUGH SSC31 9 no channel
SEP INF_LAUGH SSC26 10 no channel
SEP INF_LAUGH SSC31 10 no channel
SEP INF_LAUGH SSC37 11 no channel
SEP INF_LAUGH SSC31 11 no channel
SEP INF_LAUGH SSC21 12 no channel
SEP INF_LAUGH SSC31 12 no channel
SEP INF_LAUGH SSC37 13 no channel
SEP INF_LAUGH SSC31 13 no channel
SEP INF_LAUGH SSC31 14 no channel
SEP INF_LAUGH SSC36 15 no channel
SEP INF_LAUGH SSC31 15 no channel
SEP INF_LAUGH SSC36 16 no channel
SEP INF_LAUGH SSC31 16 no channel
SEP INF_LAUGH SSC33 17 no channel
SEP INF_LAUGH SSC31 17 no channel
SEP INF_LAUGH SSC31 18 no channel
SEP INF_LAUGH SSC31 19 no channel
SEP INF_LAUGH SSC19 19 no channel
SEP INF_LAUGH SSC31 20 no channel
SEP STATIC SSC36 1 no channel
SEP STATIC SSC36 7 no channel
SEP STATIC SSC36 8 no channel
SEP STATIC SSC26 10 no channel
SEP STATIC SSC37 11 no channel
SEP STATIC SSC21 12 no channel
SEP STATIC SSC37 13 no channel
SEP STATIC SSC36 15 no channel
SEP STATIC SSC36 16 no channel
SEP STATIC SSC33 17 no channel
SEP STATIC SSC19 19 no channel
TOG FEM_CRY SSC19 1 no channel
TOG FEM_CRY SSC38 1 no channel
TOG FEM_CRY SSC24 2 no channel
TOG FEM_CRY SSC31 2 no channel
TOG FEM_CRY SSC19 2 no channel
TOG FEM_CRY SSC19 3 no channel
TOG FEM_CRY SSC36 4 no channel
TOG FEM_CRY SSC18 4 no channel
TOG FEM_CRY SSC35 4 no channel
TOG FEM_CRY SSC19 4 no channel
TOG FEM_CRY SSC19 5 no channel
TOG FEM_CRY SSC32 6 no channel
TOG FEM_CRY SSC19 6 no channel
TOG FEM_CRY SSC25 7 no channel
TOG FEM_CRY SSC19 7 no channel
TOG FEM_CRY SSC23 8 no channel
TOG FEM_CRY SSC19 8 no channel
TOG FEM_CRY SSC19 9 no channel
TOG FEM_CRY SSC24 10 no channel
TOG FEM_CRY SSC23 10 no channel
TOG FEM_CRY SSC19 10 no channel
TOG FEM_CRY SSC19 11 no channel
TOG FEM_CRY SSC19 12 no channel
TOG FEM_CRY SSC19 13 no channel
TOG FEM_CRY SSC18 14 no channel
TOG FEM_CRY SSC19 14 no channel
TOG FEM_CRY SSC19 15 no channel
TOG FEM_CRY SSC19 16 no channel
TOG FEM_CRY SSC29 17 no channel
TOG FEM_CRY SSC35 17 no channel
TOG FEM_CRY SSC19 17 no channel
TOG FEM_CRY SSC19 18 no channel
TOG FEM_CRY SSC24 19 no channel
TOG FEM_CRY SSC21 19 no channel
TOG FEM_CRY SSC19 19 no channel
TOG FEM_CRY SSC21 20 no channel
TOG FEM_CRY SSC29 20 no channel
TOG FEM_CRY SSC18 20 no channel
TOG FEM_CRY SSC19 20 no channel
TOG FEM_LAUGH SSC19 1 no channel
TOG FEM_LAUGH SSC38 1 no channel
TOG FEM_LAUGH SSC24 2 no channel
TOG FEM_LAUGH SSC31 2 no channel
TOG FEM_LAUGH SSC19 2 no channel
TOG FEM_LAUGH SSC19 3 no channel
TOG FEM_LAUGH SSC36 4 no channel
TOG FEM_LAUGH SSC18 4 no channel
TOG FEM_LAUGH SSC35 4 no channel
TOG FEM_LAUGH SSC19 4 no channel
TOG FEM_LAUGH SSC19 5 no channel
TOG FEM_LAUGH SSC32 6 no channel
TOG FEM_LAUGH SSC19 6 no channel
TOG FEM_LAUGH SSC25 7 no channel
TOG FEM_LAUGH SSC19 7 no channel
TOG FEM_LAUGH SSC23 8 no channel
TOG FEM_LAUGH SSC19 8 no channel
TOG FEM_LAUGH SSC19 9 no channel
TOG FEM_LAUGH SSC24 10 no channel
TOG FEM_LAUGH SSC23 10 no channel
TOG FEM_LAUGH SSC19 10 no channel
TOG FEM_LAUGH SSC19 11 no channel
TOG FEM_LAUGH SSC19 12 no channel
TOG FEM_LAUGH SSC19 13 no channel
TOG FEM_LAUGH SSC18 14 no channel
TOG FEM_LAUGH SSC19 14 no channel
TOG FEM_LAUGH SSC19 15 no channel
TOG FEM_LAUGH SSC19 16 no channel
TOG FEM_LAUGH SSC29 17 no channel
TOG FEM_LAUGH SSC35 17 no channel
TOG FEM_LAUGH SSC19 17 no channel
TOG FEM_LAUGH SSC19 18 no channel
TOG FEM_LAUGH SSC24 19 no channel
TOG FEM_LAUGH SSC21 19 no channel
TOG FEM_LAUGH SSC19 19 no channel
TOG FEM_LAUGH SSC21 20 no channel
TOG FEM_LAUGH SSC29 20 no channel
TOG FEM_LAUGH SSC18 20 no channel
TOG FEM_LAUGH SSC19 20 no channel
TOG INF_CRY_HI SSC19 1 no channel
TOG INF_CRY_HI SSC38 1 no channel
TOG INF_CRY_HI SSC24 2 no channel
TOG INF_CRY_HI SSC31 2 no channel
TOG INF_CRY_HI SSC19 2 no channel
TOG INF_CRY_HI SSC19 3 no channel
TOG INF_CRY_HI SSC36 4 no channel
TOG INF_CRY_HI SSC18 4 no channel
TOG INF_CRY_HI SSC35 4 no channel
TOG INF_CRY_HI SSC19 4 no channel
TOG INF_CRY_HI SSC19 5 no channel
TOG INF_CRY_HI SSC32 6 no channel
TOG INF_CRY_HI SSC19 6 no channel
TOG INF_CRY_HI SSC25 7 no channel
TOG INF_CRY_HI SSC19 7 no channel
TOG INF_CRY_HI SSC23 8 no channel
TOG INF_CRY_HI SSC19 8 no channel
TOG INF_CRY_HI SSC19 9 no channel
TOG INF_CRY_HI SSC24 10 no channel
TOG INF_CRY_HI SSC23 10 no channel
TOG INF_CRY_HI SSC19 10 no channel
TOG INF_CRY_HI SSC19 11 no channel
TOG INF_CRY_HI SSC19 12 no channel
TOG INF_CRY_HI SSC19 13 no channel
TOG INF_CRY_HI SSC18 14 no channel
TOG INF_CRY_HI SSC19 14 no channel
TOG INF_CRY_HI SSC19 15 no channel
TOG INF_CRY_HI SSC19 16 no channel
TOG INF_CRY_HI SSC29 17 no channel
TOG INF_CRY_HI SSC35 17 no channel
TOG INF_CRY_HI SSC19 17 no channel
TOG INF_CRY_HI SSC19 18 no channel
TOG INF_CRY_HI SSC24 19 no channel
TOG INF_CRY_HI SSC21 19 no channel
TOG INF_CRY_HI SSC19 19 no channel
TOG INF_CRY_HI SSC21 20 no channel
TOG INF_CRY_HI SSC29 20 no channel
TOG INF_CRY_HI SSC18 20 no channel
TOG INF_CRY_HI SSC19 20 no channel
TOG INF_CRY_LO SSC19 1 no channel
TOG INF_CRY_LO SSC38 1 no channel
TOG INF_CRY_LO SSC24 2 no channel
TOG INF_CRY_LO SSC31 2 no channel
TOG INF_CRY_LO SSC19 2 no channel
TOG INF_CRY_LO SSC19 3 no channel
TOG INF_CRY_LO SSC36 4 no channel
TOG INF_CRY_LO SSC18 4 no channel
TOG INF_CRY_LO SSC35 4 no channel
TOG INF_CRY_LO SSC19 4 no channel
TOG INF_CRY_LO SSC19 5 no channel
TOG INF_CRY_LO SSC32 6 no channel
TOG INF_CRY_LO SSC19 6 no channel
TOG INF_CRY_LO SSC25 7 no channel
TOG INF_CRY_LO SSC19 7 no channel
TOG INF_CRY_LO SSC23 8 no channel
TOG INF_CRY_LO SSC19 8 no channel
TOG INF_CRY_LO SSC19 9 no channel
TOG INF_CRY_LO SSC24 10 no channel
TOG INF_CRY_LO SSC23 10 no channel
TOG INF_CRY_LO SSC19 10 no channel
TOG INF_CRY_LO SSC19 11 no channel
TOG INF_CRY_LO SSC19 12 no channel
TOG INF_CRY_LO SSC19 13 no channel
TOG INF_CRY_LO SSC18 14 no channel
TOG INF_CRY_LO SSC19 14 no channel
TOG INF_CRY_LO SSC19 15 no channel
TOG INF_CRY_LO SSC19 16 no channel
TOG INF_CRY_LO SSC29 17 no channel
TOG INF_CRY_LO SSC35 17 no channel
TOG INF_CRY_LO SSC19 17 no channel
TOG INF_CRY_LO SSC19 18 no channel
TOG INF_CRY_LO SSC24 19 no channel
TOG INF_CRY_LO SSC21 19 no channel
TOG INF_CRY_LO SSC19 19 no channel
TOG INF_CRY_LO SSC21 20 no channel
TOG INF_CRY_LO SSC29 20 no channel
TOG INF_CRY_LO SSC18 20 no channel
TOG INF_CRY_LO SSC19 20 no channel
TOG INF_LAUGH SSC19 1 no channel
TOG INF_LAUGH SSC38 1 no channel
TOG INF_LAUGH SSC24 2 no channel
TOG INF_LAUGH SSC31 2 no channel
TOG INF_LAUGH SSC19 2 no channel
TOG INF_LAUGH SSC19 3 no channel
TOG INF_LAUGH SSC36 4 no channel
TOG INF_LAUGH SSC18 4 no channel
TOG INF_LAUGH SSC35 4 no channel
TOG INF_LAUGH SSC19 4 no channel
TOG INF_LAUGH SSC19 5 no channel
TOG INF_LAUGH SSC32 6 no channel
TOG INF_LAUGH SSC19 6 no channel
TOG INF_LAUGH SSC25 7 no channel
TOG INF_LAUGH SSC19 7 no channel
TOG INF_LAUGH SSC23 8 no channel
TOG INF_LAUGH SSC19 8 no channel
TOG INF_LAUGH SSC19 9 no channel
TOG INF_LAUGH SSC24 10 no channel
TOG INF_LAUGH SSC23 10 no channel
TOG INF_LAUGH SSC19 10 no channel
TOG INF_LAUGH SSC19 11 no channel
TOG INF_LAUGH SSC19 12 no channel
TOG INF_LAUGH SSC19 13 no channel
TOG INF_LAUGH SSC18 14 no channel
TOG INF_LAUGH SSC19 14 no channel
TOG INF_LAUGH SSC19 15 no channel
TOG INF_LAUGH SSC19 16 no channel
TOG INF_LAUGH SSC29 17 no channel
TOG INF_LAUGH SSC35 17 no channel
TOG INF_LAUGH SSC19 17 no channel
TOG INF_LAUGH SSC19 18 no channel
TOG INF_LAUGH SSC24 19 no channel
TOG INF_LAUGH SSC21 19 no channel
TOG INF_LAUGH SSC19 19 no channel
TOG INF_LAUGH SSC21 20 no channel
TOG INF_LAUGH SSC29 20 no channel
TOG INF_LAUGH SSC18 20 no channel
TOG INF_LAUGH SSC19 20 no channel
TOG STATIC SSC19 1 no channel
TOG STATIC SSC38 1 no channel
TOG STATIC SSC24 2 no channel
TOG STATIC SSC31 2 no channel
TOG STATIC SSC19 2 no channel
TOG STATIC SSC19 3 no channel
TOG STATIC SSC36 4 no channel
TOG STATIC SSC18 4 no channel
TOG STATIC SSC35 4 no channel
TOG STATIC SSC19 4 no channel
TOG STATIC SSC19 5 no channel
TOG STATIC SSC32 6 no channel
TOG STATIC SSC19 6 no channel
TOG STATIC SSC25 7 no channel
TOG STATIC SSC19 7 no channel
TOG STATIC SSC23 8 no channel
TOG STATIC SSC19 8 no channel
TOG STATIC SSC19 9 no channel
TOG STATIC SSC24 10 no channel
TOG STATIC SSC23 10 no channel
TOG STATIC SSC19 10 no channel
TOG STATIC SSC19 11 no channel
TOG STATIC SSC19 12 no channel
TOG STATIC SSC19 13 no channel
TOG STATIC SSC18 14 no channel
TOG STATIC SSC19 14 no channel
TOG STATIC SSC19 15 no channel
TOG STATIC SSC19 16 no channel
TOG STATIC SSC29 17 no channel
TOG STATIC SSC35 17 no channel
TOG STATIC SSC19 17 no channel
TOG STATIC SSC19 18 no channel
TOG STATIC SSC24 19 no channel
TOG STATIC SSC21 19 no channel
TOG STATIC SSC19 19 no channel
TOG STATIC SSC21 20 no channel
TOG STATIC SSC29 20 no channel
TOG STATIC SSC18 20 no channel
TOG STATIC SSC19 20 no channel
'''
