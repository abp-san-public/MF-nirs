BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import numpy as np
import os

#%%
datafile = f'{BASEPATH}/data/original/SSC_TS.csv'
data = pd.read_csv(datafile)

#% get dyad ids
child_age = pd.read_csv(f'{BASEPATH}/data/metadata/child_age.csv', index_col = 0)
dyads = child_age.index

#% get sounds
sounds = data['sound'].unique()

#%%
for COND in ['SEP', 'TOG']:
    #%%
    for SOUND in sounds:
        #%%
        for DYAD in dyads:
            #%%
            for PARENT in ['M', 'F']:
                data_parent = data.query(f'cond == @COND & sound == @SOUND & ID == "{DYAD}{PARENT}"')
                idx_change = np.where(np.diff(data_parent['ts'])>1)[0]
                
                if len(idx_change) == 2: #strange data
                    i_st = 0
                    i_sp = idx_change[0]+1
                    data_1 = data_parent.iloc[i_st:i_sp]
                    data_1.drop(['ID', 'cond', 'sound'], axis=1, inplace=True)
                
                    i_st = i_sp
                    i_sp = idx_change[1]+1
                    data_2 = data_parent.iloc[i_st:i_sp]
                    data_2.drop(['ID', 'cond', 'sound'], axis=1, inplace=True)
                    
                    i_st = i_sp
                    data_3 = data_parent.iloc[i_st:]
                    data_3.drop(['ID', 'cond', 'sound'], axis=1, inplace=True)
                    
                    for CH in np.arange(1,21):            
                        data_1_ch = data_1[f'CH{CH}']
                        data_2_ch = data_2[f'CH{CH}']
                        data_3_ch = data_3[f'CH{CH}']
                        
                        if ~(data_1_ch.isna().any() | data_2_ch.isna().any() | data_3_ch.isna().any()):
                            os.makedirs(f'{BASEPATH}/data/signals/{COND}/{SOUND}/{DYAD}/{PARENT}/CH{CH}', exist_ok=True)
                            data_1_ch.to_csv(f'{BASEPATH}/data/signals/{COND}/{SOUND}/{DYAD}/{PARENT}/CH{CH}/1.csv', index=False, header=[f'CH{CH}'])
                            data_2_ch.to_csv(f'{BASEPATH}/data/signals/{COND}/{SOUND}/{DYAD}/{PARENT}/CH{CH}/2.csv', index=False, header=[f'CH{CH}'])
                            data_3_ch.to_csv(f'{BASEPATH}/data/signals/{COND}/{SOUND}/{DYAD}/{PARENT}/CH{CH}/3.csv', index=False, header=[f'CH{CH}'])
                        else:
                            print('nans', COND, SOUND, DYAD, PARENT, CH)
                        
                else:
                    print('bad ts', COND, SOUND, DYAD, PARENT, data_parent.shape)
'''
ERRORS:
nans SEP FEM_CRY SSC37 F 11
nans SEP FEM_CRY SSC37 F 13
nans SEP FEM_CRY SSC19 F 19
nans SEP FEM_CRY SSC33 F 17
nans SEP FEM_CRY SSC26 F 10
nans SEP FEM_CRY SSC21 F 12
nans SEP FEM_CRY SSC36 M 8
nans SEP FEM_CRY SSC36 M 15
nans SEP FEM_CRY SSC36 F 1
nans SEP FEM_CRY SSC36 F 7
nans SEP FEM_CRY SSC36 F 16
nans SEP FEM_LAUGH SSC37 F 11
nans SEP FEM_LAUGH SSC37 F 13
nans SEP FEM_LAUGH SSC19 F 19
bad ts SEP FEM_LAUGH SSC31 F (238, 24)
nans SEP FEM_LAUGH SSC33 F 17
nans SEP FEM_LAUGH SSC26 F 10
nans SEP FEM_LAUGH SSC21 F 12
nans SEP FEM_LAUGH SSC36 M 8
nans SEP FEM_LAUGH SSC36 M 15
nans SEP FEM_LAUGH SSC36 F 1
nans SEP FEM_LAUGH SSC36 F 7
nans SEP FEM_LAUGH SSC36 F 16
nans SEP INF_CRY_HI SSC37 F 11
nans SEP INF_CRY_HI SSC37 F 13
nans SEP INF_CRY_HI SSC19 F 19
bad ts SEP INF_CRY_HI SSC31 F (119, 24)
nans SEP INF_CRY_HI SSC33 F 17
nans SEP INF_CRY_HI SSC26 F 10
nans SEP INF_CRY_HI SSC21 F 12
nans SEP INF_CRY_HI SSC36 M 8
nans SEP INF_CRY_HI SSC36 M 15
nans SEP INF_CRY_HI SSC36 F 1
nans SEP INF_CRY_HI SSC36 F 7
nans SEP INF_CRY_HI SSC36 F 16
nans SEP INF_CRY_LO SSC37 F 11
nans SEP INF_CRY_LO SSC37 F 13
nans SEP INF_CRY_LO SSC19 F 19
nans SEP INF_CRY_LO SSC33 F 17
nans SEP INF_CRY_LO SSC26 F 10
nans SEP INF_CRY_LO SSC21 F 12
nans SEP INF_CRY_LO SSC36 M 8
nans SEP INF_CRY_LO SSC36 M 15
nans SEP INF_CRY_LO SSC36 F 1
nans SEP INF_CRY_LO SSC36 F 7
nans SEP INF_CRY_LO SSC36 F 16
nans SEP INF_LAUGH SSC37 F 11
nans SEP INF_LAUGH SSC37 F 13
nans SEP INF_LAUGH SSC19 F 19
bad ts SEP INF_LAUGH SSC31 F (238, 24)
nans SEP INF_LAUGH SSC33 F 17
nans SEP INF_LAUGH SSC26 F 10
nans SEP INF_LAUGH SSC21 F 12
nans SEP INF_LAUGH SSC36 M 8
nans SEP INF_LAUGH SSC36 M 15
nans SEP INF_LAUGH SSC36 F 1
nans SEP INF_LAUGH SSC36 F 7
nans SEP INF_LAUGH SSC36 F 16
nans SEP STATIC SSC37 F 11
nans SEP STATIC SSC37 F 13
nans SEP STATIC SSC19 F 19
nans SEP STATIC SSC33 F 17
nans SEP STATIC SSC26 F 10
nans SEP STATIC SSC21 F 12
nans SEP STATIC SSC36 M 8
nans SEP STATIC SSC36 M 15
nans SEP STATIC SSC36 F 1
nans SEP STATIC SSC36 F 7
nans SEP STATIC SSC36 F 16
nans TOG FEM_CRY SSC35 M 4
nans TOG FEM_CRY SSC35 M 17
nans TOG FEM_CRY SSC24 M 2
nans TOG FEM_CRY SSC24 M 10
nans TOG FEM_CRY SSC24 M 19
nans TOG FEM_CRY SSC38 M 1
nans TOG FEM_CRY SSC23 M 8
nans TOG FEM_CRY SSC23 M 10
bad ts TOG FEM_CRY SSC19 M (357, 24)
nans TOG FEM_CRY SSC25 F 7
nans TOG FEM_CRY SSC31 M 2
nans TOG FEM_CRY SSC29 M 17
nans TOG FEM_CRY SSC29 F 20
nans TOG FEM_CRY SSC32 M 6
nans TOG FEM_CRY SSC21 M 19
nans TOG FEM_CRY SSC21 M 20
nans TOG FEM_CRY SSC36 M 4
nans TOG FEM_CRY SSC18 M 4
nans TOG FEM_CRY SSC18 M 14
nans TOG FEM_CRY SSC18 M 20
nans TOG FEM_LAUGH SSC35 M 4
nans TOG FEM_LAUGH SSC35 M 17
nans TOG FEM_LAUGH SSC24 M 2
nans TOG FEM_LAUGH SSC24 M 10
nans TOG FEM_LAUGH SSC24 M 19
nans TOG FEM_LAUGH SSC38 M 1
nans TOG FEM_LAUGH SSC23 M 8
nans TOG FEM_LAUGH SSC23 M 10
bad ts TOG FEM_LAUGH SSC19 M (357, 24)
nans TOG FEM_LAUGH SSC25 F 7
nans TOG FEM_LAUGH SSC31 M 2
nans TOG FEM_LAUGH SSC29 M 17
nans TOG FEM_LAUGH SSC29 F 20
nans TOG FEM_LAUGH SSC32 M 6
nans TOG FEM_LAUGH SSC21 M 19
nans TOG FEM_LAUGH SSC21 M 20
nans TOG FEM_LAUGH SSC36 M 4
nans TOG FEM_LAUGH SSC18 M 4
nans TOG FEM_LAUGH SSC18 M 14
nans TOG FEM_LAUGH SSC18 M 20
nans TOG INF_CRY_HI SSC35 M 4
nans TOG INF_CRY_HI SSC35 M 17
nans TOG INF_CRY_HI SSC24 M 2
nans TOG INF_CRY_HI SSC24 M 10
nans TOG INF_CRY_HI SSC24 M 19
nans TOG INF_CRY_HI SSC38 M 1
nans TOG INF_CRY_HI SSC23 M 8
nans TOG INF_CRY_HI SSC23 M 10
bad ts TOG INF_CRY_HI SSC19 M (356, 24)
nans TOG INF_CRY_HI SSC25 F 7
nans TOG INF_CRY_HI SSC31 M 2
nans TOG INF_CRY_HI SSC29 M 17
nans TOG INF_CRY_HI SSC29 F 20
nans TOG INF_CRY_HI SSC32 M 6
nans TOG INF_CRY_HI SSC21 M 19
nans TOG INF_CRY_HI SSC21 M 20
nans TOG INF_CRY_HI SSC36 M 4
nans TOG INF_CRY_HI SSC18 M 4
nans TOG INF_CRY_HI SSC18 M 14
nans TOG INF_CRY_HI SSC18 M 20
nans TOG INF_CRY_LO SSC35 M 4
nans TOG INF_CRY_LO SSC35 M 17
nans TOG INF_CRY_LO SSC24 M 2
nans TOG INF_CRY_LO SSC24 M 10
nans TOG INF_CRY_LO SSC24 M 19
nans TOG INF_CRY_LO SSC38 M 1
nans TOG INF_CRY_LO SSC23 M 8
nans TOG INF_CRY_LO SSC23 M 10
bad ts TOG INF_CRY_LO SSC19 M (357, 24)
nans TOG INF_CRY_LO SSC25 F 7
nans TOG INF_CRY_LO SSC31 M 2
nans TOG INF_CRY_LO SSC29 M 17
nans TOG INF_CRY_LO SSC29 F 20
nans TOG INF_CRY_LO SSC32 M 6
nans TOG INF_CRY_LO SSC21 M 19
nans TOG INF_CRY_LO SSC21 M 20
nans TOG INF_CRY_LO SSC36 M 4
nans TOG INF_CRY_LO SSC18 M 4
nans TOG INF_CRY_LO SSC18 M 14
nans TOG INF_CRY_LO SSC18 M 20
nans TOG INF_LAUGH SSC35 M 4
nans TOG INF_LAUGH SSC35 M 17
nans TOG INF_LAUGH SSC24 M 2
nans TOG INF_LAUGH SSC24 M 10
nans TOG INF_LAUGH SSC24 M 19
nans TOG INF_LAUGH SSC38 M 1
nans TOG INF_LAUGH SSC23 M 8
nans TOG INF_LAUGH SSC23 M 10
bad ts TOG INF_LAUGH SSC19 M (357, 24)
nans TOG INF_LAUGH SSC25 F 7
nans TOG INF_LAUGH SSC31 M 2
nans TOG INF_LAUGH SSC29 M 17
nans TOG INF_LAUGH SSC29 F 20
nans TOG INF_LAUGH SSC32 M 6
nans TOG INF_LAUGH SSC21 M 19
nans TOG INF_LAUGH SSC21 M 20
nans TOG INF_LAUGH SSC36 M 4
nans TOG INF_LAUGH SSC18 M 4
nans TOG INF_LAUGH SSC18 M 14
nans TOG INF_LAUGH SSC18 M 20
nans TOG STATIC SSC35 M 4
nans TOG STATIC SSC35 M 17
nans TOG STATIC SSC24 M 2
nans TOG STATIC SSC24 M 10
nans TOG STATIC SSC24 M 19
nans TOG STATIC SSC38 M 1
nans TOG STATIC SSC23 M 8
nans TOG STATIC SSC23 M 10
bad ts TOG STATIC SSC19 M (357, 24)
nans TOG STATIC SSC25 F 7
nans TOG STATIC SSC31 M 2
nans TOG STATIC SSC29 M 17
nans TOG STATIC SSC29 F 20
nans TOG STATIC SSC32 M 6
nans TOG STATIC SSC21 M 19
nans TOG STATIC SSC21 M 20
nans TOG STATIC SSC36 M 4
nans TOG STATIC SSC18 M 4
nans TOG STATIC SSC18 M 14
nans TOG STATIC SSC18 M 20
'''