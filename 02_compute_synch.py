#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import physynch as ps
import numpy as np
import os
# import matplotlib.pyplot as plt

#%% custom functions
def load_datapoint(sound, dyad, parent, rep, ch, demean=False, rescale=False, t0=False):
    data = pd.read_csv(f'{DATADIR}/{sound}/{dyad}/{parent}_{rep}.csv')
    data_ch = data[ch].values
    
    data_ch_surr, _, _ = ps.surrogate_IAAFT(data_ch)
    data_ch_surr = np.convolve(data_ch_surr, np.ones(LAG)/LAG, 'same')
  
    if demean:
        data_ch = data_ch - np.mean(data_ch)
        data_ch_surr = data_ch_surr - np.mean(data_ch_surr)
    
    if rescale:
        data_ch = data_ch/np.std(data_ch)
        data_ch_surr = data_ch_surr/np.std(data_ch_surr)
    
    if t0:
        data_ch = data_ch - data_ch[0]
        data_ch_surr = data_ch_surr - data_ch_surr[0]
        
    return(data_ch, data_ch_surr)

#%% set params

#signal params
FSAMP = 7.81
DEMEAN = False
RESCALE = False
T0 = False

#synch params
LAG_SECONDS = 2
LAG = int(LAG_SECONDS*FSAMP)
NORMALIZE = True

#%%
for COND in ['SEP', 'TOG']:
    DATADIR = f'{BASEPATH}/data/signals/{COND}'
    
    #% for all sounds
    for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
        # plt.figure()
        # plt.subplot(5,4,1)
        # plt.suptitle(SOUND)
        
        dyads = os.listdir(f'{DATADIR}/{SOUND}')
    
        OUTDIR = f'{BASEPATH}/data/synch_DTW_{LAG_SECONDS}_NoNorm/{COND}/{SOUND}'
        os.makedirs(OUTDIR, exist_ok=True)
        
        #% for all channels
        for CH in np.arange(1, 21):
            
            #%
            copresence_all_rep = []
            surr_dyads_all_rep = []
            surr_signals_all_rep = []
            
            #for all repetitions
            for REP in [1,2,3]: 
                #% load all data for the rep
                data = {}
                used_dyads = []
                for DYAD in dyads:
                    data_dyad = {}
                    ch_m, ch_surr_m = load_datapoint(SOUND, DYAD, 'M', REP, f'CH{CH}', DEMEAN, RESCALE, T0)
                    ch_f, ch_surr_f = load_datapoint(SOUND, DYAD, 'F', REP, f'CH{CH}', DEMEAN, RESCALE, T0)
                
                    if ~np.isnan(ch_m).any() and ~np.isnan(ch_f).any():
                        data_dyad['M'] = ch_m
                        data_dyad['M_surr'] = ch_surr_m
                        
                        data_dyad['F'] = ch_f
                        data_dyad['F_surr'] = ch_surr_f
                        
                        data[DYAD] = data_dyad
                        used_dyads.append(DYAD)
                
                #% regroup
                group_1 = []
                group_2 = []
                for DYAD in used_dyads:
                    mother = data[DYAD]['M']
                    mother_surr = data[DYAD]['M_surr']
                    group_1.append([mother, mother_surr])
                
                    father = data[DYAD]['F']
                    father_surr = data[DYAD]['F_surr']
                    group_2.append([father, father_surr])
                
                #% compute synch
                #DTW
                copresence, surr_dyads, surr_signals = ps.compute_distances_golland(group_1, group_2, surr_mode='equal', distance='dtw', 
                                                                                          dist_pars = {'method':'Euclidean','step':'asymmetric', 'wtype':'sakoechiba', 'openend':True, 'openbegin':True, 'wsize':LAG, 'normalize': NORMALIZE})
                #CC
                # copresence, surr_dyads, surr_signals = ps.compute_distances_golland(group_1, group_2, surr_mode = 'equal', distance ='cc', 
                #                                                                     dist_pars={'lag':LAG, 'normalize':NORMALIZE})
                copresence_all_rep.append(copresence)
                surr_dyads_all_rep.append(surr_dyads)
                surr_signals_all_rep.append(surr_signals)
            
            #%
            copresence_all_rep = np.array(copresence_all_rep)
            surr_dyads_all_rep = np.array(surr_dyads_all_rep)
            surr_signals_all_rep = np.array(surr_signals_all_rep)
            
            copresence_all_pd = pd.DataFrame(copresence_all_rep.transpose(), index = used_dyads, columns=['copresence_1', 'copresence_2', 'copresence_3'])
            copresence_all_pd.to_csv(f'{OUTDIR}/CH{CH}_copresence_all.csv')
            
            surr_dyads_all_pd = pd.DataFrame(surr_dyads_all_rep.transpose(), columns=['surr_dyads_1', 'surr_dyads_2', 'surr_dyads_3'])
            surr_dyads_all_pd.to_csv(f'{OUTDIR}/CH{CH}_surrdyads_all.csv')
            
            surr_signals_all_pd = pd.DataFrame(surr_signals_all_rep.transpose(), columns=['surr_signals_1', 'surr_signals_2', 'surr_signals_3'])
            surr_signals_all_pd.to_csv(f'{OUTDIR}/CH{CH}_surrsignals_all.csv')
     
            #%
            copresence_average = np.mean(copresence_all_rep, axis = 0)
            surr_dyads_average = np.mean(surr_dyads_all_rep, axis = 0)
            surr_signals_average = np.mean(surr_signals_all_rep, axis = 0)
            
            copresence_average_pd = pd.DataFrame(copresence_average, index = used_dyads, columns=['copresence'])
            copresence_average_pd.to_csv(f'{OUTDIR}/CH{CH}_copresence.csv')
            
            surr_dyads_average_pd = pd.DataFrame(surr_dyads_average, columns=['surr_dyads'])
            surr_dyads_average_pd.to_csv(f'{OUTDIR}/CH{CH}_surrdyads.csv')
            
            surr_signals_average_pd = pd.DataFrame(surr_signals_average, columns=['surr_signals'])
            surr_signals_average_pd.to_csv(f'{OUTDIR}/CH{CH}_surrsignals.csv')
            
            # plt.subplot(5,4, CH)
            # plt.title(f'{SOUND} - CH{CH} [{COND}]')
            # plt.hist(copresence_average, color = 'b', histtype='step', density=True)
            # plt.hist(surr_dyads_average, color = 'g', histtype='step', density=True)
            # plt.hist(surr_signals_average, color = 'r', histtype='step', density=True)
