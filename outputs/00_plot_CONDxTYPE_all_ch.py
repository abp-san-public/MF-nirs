#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.formula.api import ols
import matplotlib.pyplot as plt

LAG_SECONDS = 5
SYNCH_NAME = f'DTW_{LAG_SECONDS}'

K = 1

def test(a,b, x0, x1, y, prefix, color, style):
    statistic, p = stats.mannwhitneyu(a, b, alternative='two-sided')
    
    if p<0.05:
        # text = '{} : p = {:.5f}'.format(prefix, p)
        plt.hlines(y, x0, x1, color = color, linestyle = style)
        # plt.text(x0+0.1*(x1-x0), y*1.02, text)
    print(p)

#%%
plt.figure(figsize=(16,9))
plt.subplot(4,5,1)

for CH in np.arange(1, 21):
    
    synch = {'SEP_REAL': [], 'SEP_RANDOM': [],
             'TOG_REAL': [], 'TOG_RANDOM': []}
    
    for COND in ['SEP', 'TOG']:
        for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            
            data_REAL = data_curr.loc[:,'copresence_average'].values
            data_RANDOM = data_curr.loc[:,'stimulus_average'].values

            synch[f'{COND}_REAL'].append(data_REAL)
            synch[f'{COND}_RANDOM'].append(data_RANDOM)

    synch['SEP_REAL'] = K*np.concatenate(synch['SEP_REAL'])
    synch['SEP_RANDOM'] = K*np.concatenate(synch['SEP_RANDOM'])
    
    synch['TOG_REAL'] = K*np.concatenate(synch['TOG_REAL'])
    synch['TOG_RANDOM'] = K*np.concatenate(synch['TOG_RANDOM'])
    
    plt.subplot(5,4,CH)
    plt.title(CH)
    bp = plt.boxplot([synch['SEP_RANDOM'],
                      synch['TOG_RANDOM'],
                      synch['SEP_REAL'], 
                      synch['TOG_REAL']], 
                     showfliers=False, patch_artist=True) 
    
    for patch, color in zip(bp['boxes'], ['r', 'r', 'b', 'b']):
        patch.set(edgecolor=color, linewidth=1.5)
        patch.set(facecolor = 'w')
    
    for patch, style in zip(bp['boxes'], ['--', '-', '--', '-']):
        patch.set(linestyle=style)
    
    plt.xticks([1,2,3,4], ['SEP\nRANDOM', 'TOG\nRANDOM', 'SEP\nREAL', 'TOG\nREAL'])
    
    ax = plt.gca()
    y_min, y_max = ax.get_ylim()
    plt.ylim(y_min, y_max*1.75)
    
    test(synch['SEP_RANDOM'], synch['TOG_RANDOM'], 1, 2, y_max*1.01, prefix='SEP v TOG [RANDOM]', color = 'r', style = '-')
    test(synch['SEP_REAL'], synch['TOG_REAL'], 3, 4, y_max*1.01, prefix='SEP v TOG [REAL]', color = 'b', style = '-')
    
    test(synch['SEP_RANDOM'], synch['SEP_REAL'], 1, 3, y_max*1.2, prefix='RANDOM v. REAL [SEP]', color = 'k', style = '--')
    test(synch['TOG_RANDOM'], synch['TOG_REAL'], 2, 4, y_max*1.2, prefix='RANDOM v. REAL [TOG]', color = 'k', style = '-')
    
    test(np.concatenate([synch['SEP_RANDOM'], synch['TOG_RANDOM']]),
         np.concatenate([synch['SEP_REAL'], synch['TOG_REAL']]),
         1.5, 3.5, y_max*1.4, prefix='RANDOM v. REAL', color = 'k', style='dotted')
    
    test(np.concatenate([synch['SEP_RANDOM'], synch['SEP_REAL']]),
         np.concatenate([synch['TOG_RANDOM'], synch['TOG_REAL']]),
         1.25, 3.75, y_max*1.6, prefix='SEP v. TOG', color = 'green', style='dotted')

plt.tight_layout()