#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import physynch as ps

#%% custom functions
def load_datapoint(dyad, parent, rep, ch, demean=True, rescale=False, surrogate=False):
    data = pd.read_csv(f'{DATADIR}/{dyad}/{parent}_{rep}.csv')
    data_ch = data[ch].values
  
    if surrogate:
        data_ch, _, _ = ps.surrogate_IAAFT(data_ch)
        data_ch= np.convolve(data_ch, np.ones(5)/5, 'same')
        
    if demean:
        data_ch = data_ch - np.mean(data_ch)
    
    if rescale:
        data_ch = data_ch/np.std(data_ch)
        
    return(data_ch)

#%% set params
COND = 'TOG'
SOUND = 'INF_CRY_LO'

#signal params
DEMEAN = True
RESCALE = True
SURROGATE = False

DATADIR = f'{BASEPATH}/data/signals/{COND}/{SOUND}'
dyads = os.listdir(DATADIR)

#%% plot all
for CH in np.arange(1, 21):
    plt.figure()
    plt.title(CH)
    plt.subplot(6,4,1)
            
    #% plot all
    for i_d, DYAD in enumerate(dyads):
        plt.subplot(6,4,i_d+1)
        
        for REP in [1,2,3]:
            ch_m = load_datapoint(DYAD, 'M', REP, f'CH{CH}', DEMEAN, RESCALE, SURROGATE)
            ch_f = load_datapoint(DYAD, 'F', REP, f'CH{CH}', DEMEAN, RESCALE, SURROGATE)
    
            if ~np.isnan(ch_m).any() and ~np.isnan(ch_f).any():
                
                plt.plot(ch_m, 'r')
                plt.plot(ch_f, 'b')
            else:
                print(DYAD)
    plt.show()