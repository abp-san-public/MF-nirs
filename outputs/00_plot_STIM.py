#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats
from statsmodels.stats.multitest import multipletests

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']

#%%
for CH in [3, 7, 11, 13]:
#for CH in np.arange(1, 21):
    plt.figure()
    plt.title(f'CH{CH}')
    data_ch = {}
    for SOUND in sounds:
        
        for COND in ['SEP', 'TOG']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_ch[f'{SOUND}_{COND}'] = data_curr['copresence_average'].values
            
    
    ps = []
    for i_s, SOUND in enumerate(sounds):
        
        bp = plt.boxplot([data_ch[f'{SOUND}_SEP'],
                          data_ch[f'{SOUND}_TOG']],
                          showfliers=False, patch_artist=True,
                          positions = [i_s-0.1, i_s+0.1],
                          widths = 0.15) 
        
        for patch, color in zip(bp['boxes'], ['r', 'b']):
            patch.set(facecolor = color)
            patch.set(edgecolor = color)
        
        
        _, p = stats.mannwhitneyu(data_ch[f'{SOUND}_SEP'], data_ch[f'{SOUND}_TOG'], alternative='less')
        ps.append(p)
        
        ax = plt.gca()
        ymin, ymax = ax.get_ylim()
        
    issignif, pcorr, _, _ = multipletests(ps, method = 'fdr_bh')
    
    for i_s, (issig, pcorr) in enumerate(zip(issignif, pcorr)):
        if issig:
            plt.text(i_s-0.1, ymax, 'p = {:.7f}'.format(pcorr))
    
    plt.ylim((ymin, ymax*1.2))        
    plt.xticks(np.arange(len(sounds)), sounds)
    
    plt.legend([bp["boxes"][0], bp["boxes"][1]], ['SEP', 'TOG'], loc='upper right')