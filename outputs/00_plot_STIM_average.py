#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats
from statsmodels.stats.multitest import multipletests

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']

#%%
data_all = {}
for SOUND in sounds:
    data_sound = {}
    #for CH in [3, 7, 11, 13]:
    for CH in np.arange(1, 21):
        for COND in ['SEP', 'TOG']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_sound[f'{CH}_{COND}'] = data_curr['copresence_average'].values
    
    data_all[SOUND] = data_sound

#%%
ps = []
for i_s, SOUND in enumerate(sounds):
    
    data_SEP = []
    data_TOG = []
    
    for CH in np.arange(1,21):
        data_SEP.append(data_all[SOUND][f'{CH}_SEP'])
        data_TOG.append(data_all[SOUND][f'{CH}_TOG'])
    
    data_SEP = np.concatenate(data_SEP)
    data_TOG = np.concatenate(data_TOG)
    
    bp = plt.boxplot([data_SEP,data_TOG],
                      showfliers=False, patch_artist=True,
                      positions = [i_s-0.1, i_s+0.1],
                      widths = 0.15) 
    
    for patch, color in zip(bp['boxes'], ['r', 'b']):
        patch.set(facecolor = color)
        patch.set(edgecolor = color)
    
    
    _, p = stats.mannwhitneyu(data_SEP, data_TOG, alternative='less')
    ps.append(p)
    
    ax = plt.gca()
    ymin, ymax = ax.get_ylim()
    
issignif, pcorr, _, _ = multipletests(ps, method = 'fdr_bh')

for i_s, (issig, pcorr) in enumerate(zip(issignif, pcorr)):
    if issig:
        plt.text(i_s-0.1, ymax, 'p = {:.7f}'.format(pcorr))

plt.ylim((ymin, ymax*1.2))        
plt.xticks(np.arange(len(sounds)), sounds)

plt.legend([bp["boxes"][0], bp["boxes"][1]], ['SEP', 'TOG'], loc='upper right')