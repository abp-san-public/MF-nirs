#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats
from statsmodels.stats.multitest import multipletests

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']
sound_labels = ['Adult\nFemale\nCry', 'Adult\nFemale\nLaugh', 'Infant\nCry\nHigh-pitched', 'Infant\nCry\nLow-pitched', 'Infant\nLaugh', 'Static']

#%%
plt.figure(figsize=(16, 9))
plt.subplot(2,2,1)
for i_ch, (CH, AREA) in enumerate(zip([3, 7, 11, 13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    plt.subplot(2,2,i_ch+1)
    plt.title(f'CH{CH} - {AREA}', fontsize=14)
    data_ch = {}
    for SOUND in sounds:
        
        for COND in ['SEP', 'TOG']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_ch[f'{SOUND}_{COND}'] = 10**6*data_curr['copresence_average'].values
            
    
    ps = []
    for i_s, SOUND in enumerate(sounds):
        a = data_ch[f'{SOUND}_SEP']
        b = data_ch[f'{SOUND}_TOG']
        
        bp = plt.boxplot([a, b],
                          showfliers=False, patch_artist=True,
                          positions = [i_s-0.17, i_s+0.17],
                          widths = 0.3) 
        
        for patch, color in zip(bp['boxes'], ['r', 'b']):
            patch.set(facecolor = color)
            patch.set(edgecolor = color)
        
        for fl, c in zip(bp['fliers'], ['r', 'b']):
            fl.set(marker = '.', markerfacecolor=c, markersize=4, markeredgecolor=c)
            
            
        
        
        _, p = stats.mannwhitneyu(a, b, alternative='less')
        ps.append(p)
        
        ax = plt.gca()
        ymin, ymax = ax.get_ylim()
        
    issignif, pcorr, _, _ = multipletests(ps, method = 'fdr_bh')
    
    for i_s, (issig, p_corr) in enumerate(zip(issignif, pcorr)):
        SOUND = sounds[i_s]
        a = data_ch[f'{SOUND}_SEP']
        b = data_ch[f'{SOUND}_TOG']
        if issig:
            y_pos = np.max([a.max(), b.max()])
            plt.hlines(0.275, i_s-0.15, i_s+0.15)
            
            if p_corr <0.001:
                sig = '***'
            elif p_corr <0.01:
                sig = '**'
            elif p_corr <0.05:
                sig = '*'
            plt.text(i_s, 0.28, sig, horizontalalignment='center')
    plt.ylim((-0.15, 0.4))
    plt.xticks(np.arange(len(sound_labels)), sound_labels, fontsize=12)
    plt.grid(axis='y')
    
    # plt.legend([bp["boxes"][0], bp["boxes"][1]], ['SEP', 'TOG'], loc='upper right')
plt.tight_layout()