#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import scipy.stats as stats
from statsmodels.stats.multitest import multipletests

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

#%%
SEP_v_TOG_RANDOM = []
SEP_v_TOG_REAL= []

for CH in np.arange(1, 21):
    
    synch = {'SEP_REAL': [], 'SEP_RANDOM': [],
             'TOG_REAL': [], 'TOG_RANDOM': []}
    
    for COND in ['SEP', 'TOG']:
        for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            
            data_REAL = data_curr.loc[:,'copresence_average'].values
            data_RANDOM = data_curr.loc[:,'stimulus_average'].values

            synch[f'{COND}_REAL'].append(data_REAL)
            synch[f'{COND}_RANDOM'].append(data_RANDOM)

    synch['SEP_REAL'] = np.concatenate(synch['SEP_REAL'])
    synch['SEP_RANDOM'] = np.concatenate(synch['SEP_RANDOM'])
    
    synch['TOG_REAL'] = np.concatenate(synch['TOG_REAL'])
    synch['TOG_RANDOM'] = np.concatenate(synch['TOG_RANDOM'])
    
    _, p = stats.mannwhitneyu(synch['SEP_RANDOM'], synch['TOG_RANDOM'], alternative='two-sided')
    SEP_v_TOG_RANDOM.append([10**6*np.mean(synch['SEP_RANDOM']), 
                             10**6*np.std(synch['SEP_RANDOM']), 
                             len(synch['SEP_RANDOM']), 
                             
                             10**6*np.mean(synch['TOG_RANDOM']), 
                             10**6*np.std(synch['SEP_RANDOM']), 
                             len(synch['TOG_RANDOM']),
                             p])
    
    _, p = stats.mannwhitneyu(synch['SEP_REAL'], synch['TOG_REAL'], alternative='two-sided')
    SEP_v_TOG_REAL.append([10**6*np.mean(synch['SEP_REAL']), 
                           10**6*np.std(synch['SEP_REAL']), 
                           len(synch['SEP_REAL']), 
                           10**6*np.mean(synch['TOG_REAL']), 
                           10**6*np.std(synch['TOG_REAL']), 
                           len(synch['TOG_REAL']),
                           p])
        
#%%
SEP_v_TOG_RANDOM = pd.DataFrame(np.array(SEP_v_TOG_RANDOM), columns = ['Mean_SEP', 'SD_SEP', 'N_SEP', 'Mean_TOG', 'SD_TOG', 'N_TOG', 'p'], index = np.arange(1,21))
SEP_v_TOG_REAL = pd.DataFrame(np.array(SEP_v_TOG_REAL), columns = ['Mean_SEP', 'SD_SEP', 'N_SEP', 'Mean_TOG', 'SD_TOG', 'N_TOG', 'p'], index = np.arange(1,21))

SEP_v_TOG_RANDOM.to_csv('/home/bizzego/tmp/SEPvTOG_RANDOM.csv')
SEP_v_TOG_REAL.to_csv('/home/bizzego/tmp/SEPvTOG_REAL.csv')

#%% 
print('===============================')
print(SYNCH_NAME)

is_signif, p_corr, _, _ = multipletests(SEP_v_TOG_RANDOM['p'], method = 'fdr_bh')
print('SEP v. TOG [RANDOM]')
print(SEP_v_TOG_RANDOM.iloc[is_signif,:], p_corr[is_signif])
print()

is_signif, p_corr, _, _ = multipletests(SEP_v_TOG_REAL['p'], method = 'fdr_bh')
print('SEP v. TOG [REAL]')
print(SEP_v_TOG_REAL.iloc[is_signif,:], p_corr[is_signif])
print()