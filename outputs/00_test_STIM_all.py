#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats
from statsmodels.stats.multitest import multipletests
import pingouin as pg

def p2sig(p, N_TESTS = 1):
    if p<0.001/N_TESTS:
        return('p<0.001')
    elif p<0.01/N_TESTS:
        return('p<0.01')
    elif p<0.05/N_TESTS:
        return('p<0.05')
    else:
        return('n.s.')
        
#%%
LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']

#%%
results_all = []
for CH in np.arange(1, 21):
    data_ch = {}
    for SOUND in sounds:
        for COND in ['SEP', 'TOG']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_ch[f'{SOUND}_{COND}'] = data_curr['copresence_average'].values
    
    table_ch = []
    for i_s, SOUND in enumerate(sounds):
        
        U, p = stats.mannwhitneyu(data_ch[f'{SOUND}_SEP'], data_ch[f'{SOUND}_TOG'], alternative='less')
        N = len(data_ch[f'{SOUND}_SEP'])
        table_ch.append(pd.DataFrame({'N':N, 'U':U, 'p':p}, index=[SOUND]))
    
    table_ch = pd.concat(table_ch)
        
    issignif, pcorr = pg.multicomp(table_ch['p'].values, method = 'fdr_bh')

    p_symbol = [p2sig(x) for x in pcorr]
    
    table_ch['p-corr'] = pcorr
    table_ch['p-sym'] = p_symbol
    
    results_all.append(table_ch)