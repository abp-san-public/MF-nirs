#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 14:25:58 2019

@author: bizzego
"""

#DV: synchrony
#FACTOR: condition, sound,
#COVARIATE: parental status

#IV: age of child

#for each channel:

#synch ~ age + age*cond + age*sound + parental_status

#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

SYNCH_NAME = 'CC_2'

#%%
for CH in np.arange(1, 21):
    print('----------------')
    print(CH)
    print('----------------')
    #%
    data = []
    for COND in ['SEP', 'TOG']:
        for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_curr['sound'] = np.repeat(SOUND, len(data_curr))
            data_curr['cond'] = np.repeat(COND, len(data_curr))
            data.append(data_curr)
    
    #%
    data = pd.concat(data, axis=0)
    
    lm_model = ols('stimulus_average ~ age + age*C(cond) + age*C(sound)',
                   data=data).fit()
    
    table = sm.stats.anova_lm(lm_model, typ=2) # Type 2 ANOVA DataFrame
    print(table)
    print()