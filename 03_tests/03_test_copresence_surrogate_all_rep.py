#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import scipy.stats as stat
import numpy as np
from sklearn.utils import resample

SEED = 1234
np.random.seed(SEED)

#%% custom functions
def p2sig(p, N_TESTS = 1):
    if p<0.001/N_TESTS:
        return('***')
    elif p<0.01/N_TESTS:
        return('**')
    elif p<0.05/N_TESTS:
        return('*')
    else:
        return('n.s.')
    
def test(x,y, testtype='mann'):
    if testtype == 'mann':
        X,p = stat.mannwhitneyu(x, y, alternative = 'two-sided')
    elif testtype=='kruskal':
        X,p = stat.kruskal(x,y)
        #resample(inter_v, replace=False, n_samples = 100), resample(inter_s_v, replace=False, n_samples = 100))
    elif testtype=='wilk':
        n= np.min([len(x), len(y)])
        X, p = stat.wilcoxon(resample(x, replace=False, n_samples=n), resample(y, replace=False, n_samples=n))
    sig = p2sig(p)
    return(sig, p, X, testtype)

#%% params
COND = 'SEP'
DATADIR = f'{BASEPATH}/data/synch_CC/{COND}'

#%% for all sounds
output_table = []
for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
    SOUNDDIR = f'{DATADIR}/{SOUND}'
    
    for CH in np.arange(1,21):
        copresence = pd.read_csv(f'{SOUNDDIR}/CH{CH}_copresence_all.csv', index_col=0)
        N=len(copresence)
        copresence = copresence.values.ravel()
        
        stimulus = pd.read_csv(f'{SOUNDDIR}/CH{CH}_surrdyads_all.csv', index_col=0)
        stimulus = stimulus.values.ravel()
        
        surrogate = pd.read_csv(f'{SOUNDDIR}/CH{CH}_surrsignals_all.csv', index_col=0)
        surrogate = surrogate.values.ravel()
        
        mean_copr = np.mean(copresence)
        mean_stim = np.mean(stimulus)
        # mean_surr = np.mean(surrogate.values)
        
        # sig_surr_stim, p_surr_stim, _, _ = test(surrogate, stimulus)
        sig_stim_copr, p_stim_copr, _, _ = test(stimulus, copresence)
                
        # output_table.append([COND, SOUND, CH, N, p_surr_stim, p_stim_copr, mean_surr, mean_stim, mean_copr])
        output_table.append([COND, SOUND, CH, N, mean_stim, p_stim_copr, mean_copr])

#%%
output_table_pd = pd.DataFrame(output_table, columns = ['cond', 'sound', 'channel', 'N', 'mean_stim', 'p_stim_copr', 'mean_copr'])
output_table_pd.to_csv(f'{DATADIR}/pvalues_copresence_surrogate_all.csv')