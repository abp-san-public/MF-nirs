#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import scipy.stats as stat
import numpy as np
# from statsmodels.stats.multitest import multipletests
import matplotlib.pyplot as plt

#% params
LAG_SECONDS = 5
SYNCH_NAME = f'DTW_{LAG_SECONDS}'

DATADIR = f'{BASEPATH}/data/synch/{SYNCH_NAME}'

#%%
for COND in ['SEP', 'TOG']:
    
    output_table = []
    for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:

        for CH in np.arange(1,21):
            
            synchrony = pd.read_csv(f'{DATADIR}/{COND}/{SOUND}/CH{CH}.csv', index_col=0)
            N=len(synchrony)
            
            copresence = synchrony['copresence_average']
            stimulus = synchrony['stimulus_average']
            
            mean_copr = np.mean(copresence.values)
            mean_stim = np.mean(stimulus.values)
            
            statistic, p = stat.wilcoxon(stimulus, copresence)
                    
            output_table.append([COND, SOUND, CH, N, mean_stim, p, mean_copr])
            
    #%
    output_table_pd = pd.DataFrame(output_table, columns = ['cond', 'sound', 'channel', 'N', 'stimulus', 'p', 'copresence'])
    output_table_pd.to_csv(f'{DATADIR}/pvalues_{COND}.csv')

    #%
    copresence_all = output_table_pd['copresence'].values
    stimulus_all = output_table_pd['stimulus'].values
    
    _, p_stim_copr = stat.wilcoxon(stimulus_all, copresence_all)
    
    plt.figure()
    plt.title(COND)
    plt.boxplot([stimulus_all, copresence_all], showfliers=False)
    print(p_stim_copr)