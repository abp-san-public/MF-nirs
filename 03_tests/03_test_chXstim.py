#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']
sound_labels = ['Adult\nFemale\nCry', 'Adult\nFemale\nLaugh', 'Infant\nCry\nHigh-pitched', 'Infant\nCry\nLow-pitched', 'Infant\nLaugh', 'Static']

#%%
#for i_ch, (CH, AREA) in enumerate(zip([3, 7, 11, 13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
data_all = []
for CH in np.arange(1,21):
    data_ch = []
    for SOUND in sounds:
        COND = 'TOG'
        data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
        data_curr['sound'] = np.repeat(SOUND, len(data_curr))
        data_curr['cond'] = np.repeat(COND, len(data_curr))
        data_curr['ch'] = CH
        data_all.append(data_curr)

#%%    
data_all = pd.concat(data_all, axis=0)
    
#%
lm = ols('copresence_average ~ C(ch) + C(ch)*C(sound)', data=data_all).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)
