#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import scipy.stats as stat
import numpy as np
from sklearn.utils import resample

SEED = 1234
np.random.seed(SEED)

#%% custom functions
def p2sig(p, N_TESTS = 1):
    if p<0.001/N_TESTS:
        return('***')
    elif p<0.01/N_TESTS:
        return('**')
    elif p<0.05/N_TESTS:
        return('*')
    else:
        return('n.s.')
    
def test(x,y, testtype='mann'):
    if testtype == 'mann':
        X,p = stat.mannwhitneyu(x, y, alternative = 'two-sided')
    elif testtype=='kruskal':
        X,p = stat.kruskal(x,y)
        #resample(inter_v, replace=False, n_samples = 100), resample(inter_s_v, replace=False, n_samples = 100))
    elif testtype=='wilk':
        n= np.min([len(x), len(y)])
        X, p = stat.wilcoxon(resample(x, replace=False, n_samples=n), resample(y, replace=False, n_samples=n))
    sig = p2sig(p)
    return(sig, p, X, testtype)

#%% params
DATADIR = f'{BASEPATH}/data/synch_CC'

#%% for all sounds
output_table = []
for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
    
    for CH in np.arange(1,21):
        copresence_SEP = pd.read_csv(f'{DATADIR}/SEP/{SOUND}/CH{CH}_copresence_all.csv', index_col=0)
        copresence_SEP = copresence_SEP.values.ravel()
        
        copresence_TOG = pd.read_csv(f'{DATADIR}/TOG/{SOUND}/CH{CH}_copresence_all.csv', index_col=0)
        copresence_TOG = copresence_TOG.values.ravel()
        
        mean_SEP = np.mean(copresence_SEP)
        mean_TOG = np.mean(copresence_TOG)
                
        # sig_surr_stim, p_surr_stim, _, _ = test(surrogate, stimulus)
        sig_SEP_TOG, p_SEP_TOG, _, _ = test(copresence_SEP, copresence_TOG)
                
        # output_table.append([COND, SOUND, CH, N, p_surr_stim, p_stim_copr, mean_surr, mean_stim, mean_copr])
        output_table.append([SOUND, CH, mean_SEP, p_SEP_TOG, mean_TOG])

#%%
output_table_pd = pd.DataFrame(output_table, columns = ['sound', 'channel', 'mean_SEP', 'p_SEP_TOG', 'mean_TOG'])
output_table_pd.to_csv(f'{DATADIR}/pvalues_SEP_TOG_all.csv')