#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.formula.api import ols
import scipy.stats as stats
import matplotlib.pyplot as plt

def place_text(text, x=None, y=None):
    ax = plt.gca()
    
    if x is None:
        xmin, xmax = ax.get_xlim()
        x = xmin + (xmax - xmin)/2
        
    if y is None:
        ymin, ymax = ax.get_ylim()
        y = ymin + 0.8*(ymax - ymin)
        
    plt.text(x, y, text, horizontalalignment='center')

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']
sound_labels = ['Adult\nFemale\nCry', 'Adult\nFemale\nLaugh', 'Infant\nCry\nHigh-pitched', 'Infant\nCry\nLow-pitched', 'Infant\nLaugh', 'Static']

metadata = pd.read_csv(f'{BASEPATH}/data/metadata/metadata.csv', index_col=0)

#%%
data_all = []
for i_ch, (CH, AREA) in enumerate(zip([3, 7, 11, 13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    for SOUND in sounds:
        for COND in ['SEP', 'TOG']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_curr = data_curr.join(metadata)
            data_curr['sound'] = np.repeat(SOUND, len(data_curr))
            data_curr['cond'] = np.repeat(COND, len(data_curr))
            data_curr['channel'] = np.repeat(CH, len(data_curr))
            data_all.append(data_curr)

data_all = pd.concat(data_all, axis = 0)

data_all['dyad'] = data_all.index
data_all['parent_average_age'] = data_all.loc[:,['Age_female', 'Age_male']].mean(axis=1)

#%% 
data_TOG = data_all.query('cond == "TOG"')
data_SEP = data_all.query('cond == "SEP"')

#%%
#primi/multi
lm = ols('copresence_average ~ C(channel) + C(Multi) + C(cond) + C(Multi)*C(cond)', data=data_all).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)

#
mw_SEP, p_mw_SEP = stats.mannwhitneyu(data_SEP.query('Multi == "N"')['copresence_average'], data_SEP.query('Multi == "Y"')['copresence_average'])
mw_TOG, p_mw_TOG = stats.mannwhitneyu(data_TOG.query('Multi == "N"')['copresence_average'], data_TOG.query('Multi == "Y"')['copresence_average'])

ax1 = plt.subplot(5,1,1)
plt.boxplot([data_SEP.query('Multi == "N"')['copresence_average'], data_SEP.query('Multi == "Y"')['copresence_average'],
             data_TOG.query('Multi == "N"')['copresence_average'], data_TOG.query('Multi == "Y"')['copresence_average']], 
            showfliers=False, labels = ['SEP_Primi', 'SEP_Multi', 'TOG_Primi', 'TOG_Multi'])

place_text('p = {:.5f}'.format(p_mw_SEP), x=1.5)

place_text('p = {:.5f}'.format(p_mw_TOG), x=3.5)

plt.ylabel('all channels')

for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    plt.subplot(5,1,i_ch+2)
    mw_SEP, p_mw_SEP = stats.mannwhitneyu(data_SEP.query(f'Multi == "N" and channel == {CH}')['copresence_average'], 
                                          data_SEP.query(f'Multi == "Y" and channel == {CH}')['copresence_average'])
    mw_TOG, p_mw_TOG = stats.mannwhitneyu(data_TOG.query(f'Multi == "N" and channel == {CH}')['copresence_average'], 
                                          data_TOG.query(f'Multi == "Y" and channel == {CH}')['copresence_average'])

    plt.boxplot([data_SEP.query(f'Multi == "N" and channel == {CH}')['copresence_average'], 
                 data_SEP.query(f'Multi == "Y" and channel == {CH}')['copresence_average'],
                 data_TOG.query(f'Multi == "N" and channel == {CH}')['copresence_average'], 
                 data_TOG.query(f'Multi == "Y" and channel == {CH}')['copresence_average']], 
                 showfliers=False, labels = ['SEP_Primi', 'SEP_Multi', 'TOG_Primi', 'TOG_Multi'])
    
    place_text('p = {:.5f}'.format(p_mw_SEP), x=1.5)
    place_text('p = {:.5f}'.format(p_mw_TOG), x=3.5)

    plt.ylabel(f'ch: {CH} - {AREA}')
    
#%%
#Age child #NOT INTERESTING
lm = ols('copresence_average ~ Age_Child + C(cond)', data=data_all).fit()
lm = ols('copresence_average ~ Age_Child + C(cond)', data=data_all.query('Multi == "Y"')).fit()
lm = ols('copresence_average ~ Age_Child + C(cond)', data=data_all.query('Multi == "N"')).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)

plt.plot(data_all['Age_Child'], data_all['copresence_average'], '.')

corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP['Age_Child'], data_SEP['copresence_average'])
corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG['Age_Child'], data_TOG['copresence_average'])


#%% PARENT RATIO
lm = ols('copresence_average ~ ratio_average + C(cond) + ratio_average*C(cond)', data=data_all).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)

corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP['ratio_average'], data_SEP['copresence_average'])
corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG['ratio_average'], data_TOG['copresence_average'])

#%
ax1 = plt.subplot(5,2,1)
plt.plot(data_SEP['ratio_average'], data_SEP['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
plt.title('SEP')
plt.ylabel('all_channels')

plt.subplot(5,2,2, sharey=ax1)
plt.plot(data_TOG['ratio_average'], data_TOG['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
plt.title('TOG')


for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP.query('channel == @CH')['ratio_average'], 
                                           data_SEP.query('channel == @CH')['copresence_average'])
    
    corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG.query('channel == @CH')['ratio_average'], 
                                           data_TOG.query('channel == @CH')['copresence_average'])
    
    #%
    plt.subplot(5,2,2*i_ch+3)
    plt.plot(data_SEP.query('channel == @CH')['ratio_average'],
             data_SEP.query('channel == @CH')['copresence_average'], '.')
    
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
    plt.title('SEP')
    plt.ylabel(f'ch: {CH} - {AREA}')
    
    plt.subplot(5,2,2*i_ch+4, sharey=ax1)
    plt.plot(data_TOG.query('channel == @CH')['ratio_average'], 
             data_TOG.query('channel == @CH')['copresence_average'], '.')
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
    plt.title('TOG')
    
#%% PARENT AGE
lm = ols('copresence_average ~ parent_average_age + C(cond) + parent_average_age*C(cond)', data=data_all).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)

corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP['parent_average_age'], data_SEP['copresence_average'])
corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG['parent_average_age'], data_TOG['copresence_average'])

#%
ax1 = plt.subplot(5,2,1)
plt.plot(data_SEP['parent_average_age'], data_SEP['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
plt.title('SEP')
plt.ylabel('all_channels')

plt.subplot(5,2,2, sharey=ax1)
plt.plot(data_TOG['parent_average_age'], data_TOG['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
plt.title('TOG')


for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP.query('channel == @CH')['parent_average_age'], 
                                           data_SEP.query('channel == @CH')['copresence_average'])
    
    corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG.query('channel == @CH')['parent_average_age'], 
                                           data_TOG.query('channel == @CH')['copresence_average'])
    
    #%
    plt.subplot(5,2,2*i_ch+3)
    plt.plot(data_SEP.query('channel == @CH')['parent_average_age'],
             data_SEP.query('channel == @CH')['copresence_average'], '.')
    
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
    plt.title('SEP')
    plt.ylabel(f'ch: {CH} - {AREA}')
    
    plt.subplot(5,2,2*i_ch+4, sharey=ax1)
    plt.plot(data_TOG.query('channel == @CH')['parent_average_age'], 
             data_TOG.query('channel == @CH')['copresence_average'], '.')
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
    plt.title('TOG')
    
#%% mother AGE
lm = ols('copresence_average ~ Age_female + C(cond) + Age_female*C(cond)', data=data_all).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)

corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP['Age_female'], data_SEP['copresence_average'])
corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG['Age_female'], data_TOG['copresence_average'])

#%
ax1 = plt.subplot(5,2,1)
plt.plot(data_SEP['Age_female'], data_SEP['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
plt.title('SEP')
plt.ylabel('all_channels')

plt.subplot(5,2,2, sharey=ax1)
plt.plot(data_TOG['Age_female'], data_TOG['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
plt.title('TOG')


for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP.query('channel == @CH')['Age_female'], 
                                           data_SEP.query('channel == @CH')['copresence_average'])
    
    corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG.query('channel == @CH')['Age_female'], 
                                           data_TOG.query('channel == @CH')['copresence_average'])
    
    #%
    plt.subplot(5,2,2*i_ch+3)
    plt.plot(data_SEP.query('channel == @CH')['Age_female'],
             data_SEP.query('channel == @CH')['copresence_average'], '.')
    
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
    plt.title('SEP')
    plt.ylabel(f'ch: {CH} - {AREA}')
    
    plt.subplot(5,2,2*i_ch+4, sharey=ax1)
    plt.plot(data_TOG.query('channel == @CH')['Age_female'], 
             data_TOG.query('channel == @CH')['copresence_average'], '.')
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
    plt.title('TOG')

#%% father AGE
lm = ols('copresence_average ~ Age_male + C(cond) + C(Multi) + Age_male*C(cond)', data=data_all).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
print(table)

corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP['Age_male'], data_SEP['copresence_average'])
corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG['Age_male'], data_TOG['copresence_average'])

#%
ax1 = plt.subplot(5,2,1)
plt.plot(data_SEP['Age_male'], data_SEP['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
plt.title('SEP')
plt.ylabel('all_channels')

plt.subplot(5,2,2, sharey=ax1)
plt.plot(data_TOG['Age_male'], data_TOG['copresence_average'], '.')
place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
plt.title('TOG')


for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    corr_SEP, p_corr_SEP = stats.spearmanr(data_SEP.query('channel == @CH')['Age_male'], 
                                           data_SEP.query('channel == @CH')['copresence_average'])
    
    corr_TOG, p_corr_TOG = stats.spearmanr(data_TOG.query('channel == @CH')['Age_male'], 
                                           data_TOG.query('channel == @CH')['copresence_average'])
    
    #%
    plt.subplot(5,2,2*i_ch+3)
    plt.plot(data_SEP.query('channel == @CH')['Age_male'],
             data_SEP.query('channel == @CH')['copresence_average'], '.')
    
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_SEP, p_corr_SEP))
    plt.title('SEP')
    plt.ylabel(f'ch: {CH} - {AREA}')
    
    plt.subplot(5,2,2*i_ch+4, sharey=ax1)
    plt.plot(data_TOG.query('channel == @CH')['Age_male'], 
             data_TOG.query('channel == @CH')['copresence_average'], '.')
    place_text('rho = {:.3f}, p = {:.5f}'.format(corr_TOG, p_corr_TOG))
    plt.title('TOG')
