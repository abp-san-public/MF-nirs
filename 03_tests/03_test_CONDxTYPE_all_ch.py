#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import scipy.stats as stats
from statsmodels.stats.multitest import multipletests
import matplotlib.pyplot as plt

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

#%%
SEP_v_TOG_RANDOM = []
SEP_v_TOG_REAL= []
RANDOM_v_REAL_SEP = []
RANDOM_v_REAL_TOG = []
SEP_v_TOG = []
RANDOM_v_REAL = []

for CH in np.arange(1, 21):
    
    synch = {'SEP_REAL': [], 'SEP_RANDOM': [],
             'TOG_REAL': [], 'TOG_RANDOM': []}
    
    for COND in ['SEP', 'TOG']:
        for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            
            data_REAL = data_curr.loc[:,'copresence_average'].values
            data_RANDOM = data_curr.loc[:,'stimulus_average'].values

            synch[f'{COND}_REAL'].append(data_REAL)
            synch[f'{COND}_RANDOM'].append(data_RANDOM)

    synch['SEP_REAL'] = np.concatenate(synch['SEP_REAL'])
    synch['SEP_RANDOM'] = np.concatenate(synch['SEP_RANDOM'])
    
    synch['TOG_REAL'] = np.concatenate(synch['TOG_REAL'])
    synch['TOG_RANDOM'] = np.concatenate(synch['TOG_RANDOM'])
    
    _, p = stats.mannwhitneyu(synch['SEP_RANDOM'], synch['TOG_RANDOM'], alternative='two-sided')
    SEP_v_TOG_RANDOM.append([np.mean(synch['SEP_RANDOM']), p, np.mean(synch['TOG_RANDOM'])])
    
    _, p = stats.mannwhitneyu(synch['SEP_REAL'], synch['TOG_REAL'], alternative='two-sided')
    SEP_v_TOG_REAL.append([np.mean(synch['SEP_REAL']), p, np.mean(synch['TOG_REAL'])])
        
    _, p = stats.mannwhitneyu(synch['SEP_RANDOM'], synch['SEP_REAL'], alternative='two-sided')
    RANDOM_v_REAL_SEP.append([np.mean(synch['SEP_RANDOM']), p, np.mean(synch['SEP_REAL'])])
    
    _, p = stats.mannwhitneyu(synch['TOG_RANDOM'], synch['TOG_REAL'], alternative='two-sided')
    RANDOM_v_REAL_TOG.append([np.mean(synch['TOG_RANDOM']), p, np.mean(synch['TOG_REAL'])])
    
    a = np.concatenate([synch['SEP_RANDOM'], synch['TOG_RANDOM']])
    b = np.concatenate([synch['SEP_REAL'], synch['TOG_REAL']])
    _, p = stats.mannwhitneyu(a, b, alternative='two-sided')
    RANDOM_v_REAL.append([np.mean(a), p, np.mean(b)])
    
    a = np.concatenate([synch['SEP_RANDOM'], synch['SEP_REAL']])
    b = np.concatenate([synch['TOG_RANDOM'], synch['TOG_REAL']])
    _, p = stats.mannwhitneyu(a, b, alternative = 'two-sided')
    SEP_v_TOG.append([np.mean(a), p, np.mean(b)])
    
#%%
SEP_v_TOG_RANDOM = pd.DataFrame(np.array(SEP_v_TOG_RANDOM), columns = ['SEP', 'p', 'TOG'], index = np.arange(1,21))
SEP_v_TOG_REAL = pd.DataFrame(np.array(SEP_v_TOG_REAL), columns = ['SEP', 'p', 'TOG'], index = np.arange(1,21))

RANDOM_v_REAL_SEP = pd.DataFrame( np.array( RANDOM_v_REAL_SEP ), columns = ['RANDOM', 'p', 'REAL'], index = np.arange(1,21))
RANDOM_v_REAL_TOG = pd.DataFrame( np.array( RANDOM_v_REAL_TOG ), columns = ['RANDOM', 'p', 'REAL'], index = np.arange(1,21))
RANDOM_v_REAL = pd.DataFrame( np.array( RANDOM_v_REAL ), columns = ['RANDOM', 'p', 'REAL'], index = np.arange(1,21))
SEP_v_TOG = pd.DataFrame( np.array(SEP_v_TOG  ), columns = ['SEP', 'p', 'TOG'], index = np.arange(1,21))

#%% 
print('===============================')
print(SYNCH_NAME)

is_signif, p_corr, _, _ = multipletests(SEP_v_TOG_RANDOM['p'], method = 'fdr_bh')
print('SEP v. TOG [RANDOM]')
print(SEP_v_TOG_RANDOM.iloc[is_signif,:])
print()

is_signif, p_corr, _, _ = multipletests(SEP_v_TOG_REAL['p'], method = 'fdr_bh')
print('SEP v. TOG [REAL]')
print(SEP_v_TOG_REAL.iloc[is_signif,:])
print()

is_signif, p_corr, _, _ = multipletests(SEP_v_TOG['p'], method = 'fdr_bh')
print('SEP v. TOG')
print(SEP_v_TOG.iloc[is_signif,:])
print()

is_signif, p_corr, _, _ = multipletests(RANDOM_v_REAL_SEP['p'], method = 'fdr_bh')
print('RANDOM v. REAL [SEP]')
print(RANDOM_v_REAL_SEP.iloc[is_signif,:])
print()

is_signif, p_corr, _, _ = multipletests(RANDOM_v_REAL_TOG['p'], method = 'fdr_bh')
print('RANDOM v. REAL [TOG]')
print(RANDOM_v_REAL_TOG.iloc[is_signif,:])
print()

is_signif, p_corr, _, _ = multipletests(RANDOM_v_REAL['p'], method = 'fdr_bh')
print('RANDOM v. REAL')
print(RANDOM_v_REAL.iloc[is_signif,:])
print()