#%% import
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'
import pandas as pd
import scipy.stats as stat
import numpy as np

import matplotlib.pyplot as plt

#% params
LAG_SECONDS = 5
SYNCH_NAME = f'DTW_{LAG_SECONDS}'

DATADIR = f'{BASEPATH}/data/synch/{SYNCH_NAME}'

#%% for all sounds
output_table = []
for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']:
    
    for CH in np.arange(1,21):
        copresence_SEP = pd.read_csv(f'{DATADIR}/SEP/{SOUND}/CH{CH}.csv', index_col=0)
        copresence_SEP = copresence_SEP['copresence_average'].values
        
        copresence_TOG = pd.read_csv(f'{DATADIR}/TOG/{SOUND}/CH{CH}.csv', index_col=0)
        copresence_TOG = copresence_TOG['copresence_average'].values
        
        mean_SEP = np.mean(copresence_SEP)
        mean_TOG = np.mean(copresence_TOG)
                
        statistic, p = stat.mannwhitneyu(copresence_SEP, copresence_TOG, alternative='two-sided')
                
        # output_table.append([COND, SOUND, CH, N, p_surr_stim, p_stim_copr, mean_surr, mean_stim, mean_copr])
        output_table.append([SOUND, CH, mean_SEP, p, mean_TOG])

#%
output_table_pd = pd.DataFrame(output_table, columns = ['sound', 'channel', 'mean_SEP', 'p_SEP_TOG', 'mean_TOG'])
output_table_pd.to_csv(f'{DATADIR}/pvalues_SEP_TOG.csv')

#%%
SEP_all = output_table_pd['mean_SEP'].values
TOG_all = output_table_pd['mean_TOG'].values

_, p_SEP_TOG = stat.wilcoxon(SEP_all, TOG_all)

plt.boxplot([SEP_all, TOG_all], showfliers=False)
print(p_SEP_TOG)

# plt.hist(SEP_all, histtype='step', density=True)
# plt.hist(TOG_all, histtype='step', density=True)