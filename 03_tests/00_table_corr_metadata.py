#%%
BASEPATH = '/home/bizzego/UniTn/projects/MC_synch'

import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols
import scipy.stats as stats

from statsmodels.stats.multitest import multipletests

LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']
sound_labels = ['Adult\nFemale\nCry', 'Adult\nFemale\nLaugh', 'Infant\nCry\nHigh-pitched', 'Infant\nCry\nLow-pitched', 'Infant\nLaugh', 'Static']

metadata = pd.read_csv(f'{BASEPATH}/data/metadata/metadata.csv', index_col=0)

OUTDIR = f'{BASEPATH}/data'

#%%
data_all = []
for i_ch, (CH, AREA) in enumerate(zip([3, 7, 11, 13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    for SOUND in sounds:
        for COND in ['SEP', 'TOG']:
            data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/{COND}/{SOUND}/CH{CH}.csv', index_col = 0)
            data_curr = data_curr.join(metadata)
            data_curr['sound'] = np.repeat(SOUND, len(data_curr))
            data_curr['cond'] = np.repeat(COND, len(data_curr))
            data_curr['channel'] = np.repeat(CH, len(data_curr))
            data_all.append(data_curr)

data_all = pd.concat(data_all, axis = 0)

data_all['dyad'] = data_all.index
data_all['parent_average_age'] = data_all.loc[:,['Age_female', 'Age_male']].mean(axis=1)

data_all['copresence_average'] = 10**6*data_all['copresence_average']

#%% 
data_TOG = data_all.query('cond == "TOG"')
data_SEP = data_all.query('cond == "SEP"')

#%% PRIMI/MULTI
def get_primi_multi(data):
    primi = data.query('Multi == "N"')['copresence_average']
    multi = data.query('Multi == "Y"')['copresence_average']
    mean_primi, std_primi = np.mean(primi), np.std(primi)
    mean_multi, std_multi = np.mean(multi), np.std(multi)
    mw_statistic, p = stats.mannwhitneyu(primi, multi)
    return({'Mean Primi': mean_primi, 
            'SD Primi': std_primi, 
            'M-W statistic': mw_statistic, 
            'p-value': p, 
            'Mean Multi': mean_multi, 
            'SD Multi':std_multi})

#ALL CHANNELS
all_results_SEP = []
all_results_TOG = []

all_results_SEP.append(pd.DataFrame(get_primi_multi(data_SEP), index = ['all']))
all_results_TOG.append(pd.DataFrame(get_primi_multi(data_TOG), index = ['all']))

for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    all_results_SEP.append(pd.DataFrame(get_primi_multi(data_SEP.query('channel == @CH')), index = [CH]))
    all_results_TOG.append(pd.DataFrame(get_primi_multi(data_TOG.query('channel == @CH')), index = [CH]))

all_results_SEP = pd.concat(all_results_SEP)
p_channels = all_results_SEP['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_SEP['p-value'].iloc[1:] = p_corrected

all_results_TOG = pd.concat(all_results_TOG)
p_channels = all_results_TOG['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_TOG['p-value'].iloc[1:] = p_corrected

all_results = pd.merge(all_results_SEP, all_results_TOG, left_index=True, right_index=True, suffixes=['_SEP', '_TOG'])

all_results.to_csv(f'{OUTDIR}/multi_primi.csv')

#%% PARENT RATIO
def get_parent_ratio(data):
    rho, p = stats.spearmanr(data['ratio_average'], data['copresence_average'])
    return({'Rho':rho, 'p-value':p})
    
#ALL CHANNELS
all_results_SEP = []
all_results_TOG = []

all_results_SEP.append(pd.DataFrame(get_parent_ratio(data_SEP), index = ['all']))
all_results_TOG.append(pd.DataFrame(get_parent_ratio(data_TOG), index = ['all']))

for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    all_results_SEP.append(pd.DataFrame(get_parent_ratio(data_SEP.query('channel == @CH')), index = [CH]))
    all_results_TOG.append(pd.DataFrame(get_parent_ratio(data_TOG.query('channel == @CH')), index = [CH]))

all_results_SEP = pd.concat(all_results_SEP)
p_channels = all_results_SEP['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_SEP['p-value'].iloc[1:] = p_corrected

all_results_TOG = pd.concat(all_results_TOG)
p_channels = all_results_TOG['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_TOG['p-value'].iloc[1:] = p_corrected

all_results = pd.merge(all_results_SEP, all_results_TOG, left_index=True, right_index=True, suffixes=['_SEP', '_TOG'])

all_results.to_csv(f'{OUTDIR}/parent_ratio.csv')

#%% PARENT AVERAGE AGE
def get_parent_age(data):
    rho, p = stats.spearmanr(data['parent_average_age'], data['copresence_average'])
    return({'Rho':rho, 'p-value':p})
    
#ALL CHANNELS
all_results_SEP = []
all_results_TOG = []

all_results_SEP.append(pd.DataFrame(get_parent_age(data_SEP), index = ['all']))
all_results_TOG.append(pd.DataFrame(get_parent_age(data_TOG), index = ['all']))

for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    all_results_SEP.append(pd.DataFrame(get_parent_age(data_SEP.query('channel == @CH')), index = [CH]))
    all_results_TOG.append(pd.DataFrame(get_parent_age(data_TOG.query('channel == @CH')), index = [CH]))

all_results_SEP = pd.concat(all_results_SEP)
p_channels = all_results_SEP['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_SEP['p-value'].iloc[1:] = p_corrected

all_results_TOG = pd.concat(all_results_TOG)
p_channels = all_results_TOG['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_TOG['p-value'].iloc[1:] = p_corrected

all_results = pd.merge(all_results_SEP, all_results_TOG, left_index=True, right_index=True, suffixes=['_SEP', '_TOG'])

all_results.to_csv(f'{OUTDIR}/parent_average_age.csv')
    
#%% MOTHER AGE
def get_mother_age(data):
    rho, p = stats.spearmanr(data['Age_female'], data['copresence_average'])
    return({'Rho':rho, 'p-value':p})
    
#ALL CHANNELS
all_results_SEP = []
all_results_TOG = []

all_results_SEP.append(pd.DataFrame(get_mother_age(data_SEP), index = ['all']))
all_results_TOG.append(pd.DataFrame(get_mother_age(data_TOG), index = ['all']))

for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    all_results_SEP.append(pd.DataFrame(get_mother_age(data_SEP.query('channel == @CH')), index = [CH]))
    all_results_TOG.append(pd.DataFrame(get_mother_age(data_TOG.query('channel == @CH')), index = [CH]))

all_results_SEP = pd.concat(all_results_SEP)
p_channels = all_results_SEP['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_SEP['p-value'].iloc[1:] = p_corrected

all_results_TOG = pd.concat(all_results_TOG)
p_channels = all_results_TOG['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_TOG['p-value'].iloc[1:] = p_corrected

all_results = pd.merge(all_results_SEP, all_results_TOG, left_index=True, right_index=True, suffixes=['_SEP', '_TOG'])

all_results.to_csv(f'{OUTDIR}/mother_age.csv')

#%% FATHER AGE
def get_father_age(data):
    rho, p = stats.spearmanr(data['Age_male'], data['copresence_average'])
    return({'Rho':rho, 'p-value':p})
    
#ALL CHANNELS
all_results_SEP = []
all_results_TOG = []

all_results_SEP.append(pd.DataFrame(get_father_age(data_SEP), index = ['all']))
all_results_TOG.append(pd.DataFrame(get_father_age(data_TOG), index = ['all']))

for i_ch, (CH, AREA) in enumerate(zip([3,7,11,13], ['IFG', 'MFG', 'aPFC', 'aPFC'])):
    all_results_SEP.append(pd.DataFrame(get_father_age(data_SEP.query('channel == @CH')), index = [CH]))
    all_results_TOG.append(pd.DataFrame(get_father_age(data_TOG.query('channel == @CH')), index = [CH]))

all_results_SEP = pd.concat(all_results_SEP)
p_channels = all_results_SEP['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_SEP['p-value'].iloc[1:] = p_corrected

all_results_TOG = pd.concat(all_results_TOG)
p_channels = all_results_TOG['p-value'].values[1:]
issignif, p_corrected, _, _ = multipletests(p_channels, method = 'fdr_bh')
all_results_TOG['p-value'].iloc[1:] = p_corrected

all_results = pd.merge(all_results_SEP, all_results_TOG, left_index=True, right_index=True, suffixes=['_SEP', '_TOG'])

all_results.to_csv(f'{OUTDIR}/father_age.csv')