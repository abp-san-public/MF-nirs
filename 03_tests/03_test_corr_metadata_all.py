#%%
BASEPATH = '/home/bizzego/UniTn/projects/MF_nirs'

import numpy as np
import pandas as pd
import pingouin as pg
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.formula.api import ols

def p2sig(p, N_TESTS = 1):
    if p<0.001/N_TESTS:
        return('p<0.001')
    elif p<0.01/N_TESTS:
        return('p<0.01')
    elif p<0.05/N_TESTS:
        return('p<0.05')
    else:
        return('n.s.')

def create_anova_table(aov):
    aov['mean_sq'] = aov[:]['sum_sq']/aov[:]['df']
    aov['eta_sq'] = aov[:-1]['sum_sq']/sum(aov['sum_sq'])
    aov['omega_sq'] = (aov[:-1]['sum_sq']-(aov[:-1]['df']*aov['mean_sq'][-1]))/(sum(aov['sum_sq'])+aov['mean_sq'][-1])
    cols = ['sum_sq', 'df', 'mean_sq', 'F', 'PR(>F)', 'eta_sq', 'omega_sq']
    aov = aov[cols]
    return(aov)

#%%        
LAG_SECONDS = 2
SYNCH_NAME = f'CC_{LAG_SECONDS}'

sounds = ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH', 'STATIC']
sound_labels = ['Adult\nFemale\nCry', 'Adult\nFemale\nLaugh', 'Infant\nCry\nHigh-pitched', 'Infant\nCry\nLow-pitched', 'Infant\nLaugh', 'Static']

metadata = pd.read_csv(f'{BASEPATH}/data/metadata/metadata.csv', index_col=0)

OUTDIR_TEST = f'{BASEPATH}/data/synch/{SYNCH_NAME}'

#%%
data_TOG = []
for CH in np.arange(1,21):
    for SOUND in sounds:
        data_curr = pd.read_csv(f'{BASEPATH}/data/synch/{SYNCH_NAME}/TOG/{SOUND}/CH{CH}.csv', index_col = 0)
        data_curr = data_curr.join(metadata)
        data_curr['sound'] = np.repeat(SOUND, len(data_curr))
        data_curr['channel'] = np.repeat(CH, len(data_curr))
        data_TOG.append(data_curr)

data_TOG = pd.concat(data_TOG, axis = 0)

data_TOG['dyad'] = data_TOG.index
data_TOG['parent_average_age'] = data_TOG.loc[:,['Age_female', 'Age_male']].mean(axis=1)

data_TOG['copresence_average'] = 10**6*data_TOG['copresence_average'].values

#%% MULTI/PRIMI
lm = ols('copresence_average ~ C(Multi) + C(channel) + C(Multi)*C(channel)', data=data_TOG).fit()
#    print(lm.summary())
table = sm.stats.anova_lm(lm, typ=2)
table= create_anova_table(table)

table['p-sym']  = [p2sig(x) for x in table['PR(>F)']]
table.to_csv(f'{OUTDIR_TEST}/anova_multi.csv', float_format = '%.3f')

table_ch = []
for CH in np.arange(1,21):
    data_ch = data_TOG.query('channel ==@CH')
    
    data_multi = data_ch.query('Multi == "N"')['copresence_average']
    N_multi = data_multi.shape[0]
    mean_multi = np.mean(data_multi)
    std_multi = np.std(data_multi)
    
    data_primi = data_ch.query('Multi == "Y"')['copresence_average']
    N_primi= data_primi.shape[0]
    mean_primi = np.mean(data_primi)
    std_primi = np.std(data_primi)
    
    mw_TOG, p_mw_TOG = stats.mannwhitneyu(data_multi, data_primi )
    
    table_ch.append(pd.DataFrame({'N_multi': N_multi, 'mean_multi': mean_multi, 'SD_multi': std_multi,
                                  'N_primi': N_primi, 'mean_primi': mean_primi, 'SD_primi':std_primi,
                                  'U': mw_TOG, 'p-unc':p_mw_TOG}, index=[CH]))

table_ch = pd.concat(table_ch)
    
issignif, pcorr = pg.multicomp(table_ch['p-unc'].values, method = 'fdr_bh')

p_symbol = [p2sig(x) for x in pcorr]

table_ch['p-corr'] = pcorr
table_ch['p-sym'] = p_symbol
table_ch.to_csv(f'{OUTDIR_TEST}/posthoc_multi.csv', float_format = '%.3f')


#%% correlations
for iv in ['ratio_average', 'parent_average_age', 'Age_female', 'Age_male']:
    lm = ols(f'copresence_average ~ {iv} + C(channel) + {iv}*C(channel)', data=data_TOG).fit()
    #    print(lm.summary())
    table = sm.stats.anova_lm(lm, typ=2)
    
    table['p-sym']  = [p2sig(x) for x in table['PR(>F)']]
    table.to_csv(f'{OUTDIR_TEST}/anova_{iv}.csv', float_format = '%.3f')
        
    table_ch = []
    for CH in np.arange(1,21):
        data_ch = data_TOG.query('channel ==@CH')
        N = data_ch.shape[0]
        
        corr_TOG, p_corr_TOG = stats.spearmanr(data_ch[iv], data_ch['copresence_average'])
        
        table_ch.append(pd.DataFrame({'N': N, 'rho': corr_TOG, 'p-unc':p_corr_TOG}, index=[CH]))
    
    table_ch = pd.concat(table_ch)
        
    issignif, pcorr = pg.multicomp(table_ch['p-unc'].values, method = 'fdr_bh')
    
    p_symbol = [p2sig(x) for x in pcorr]
    
    table_ch['p-corr'] = pcorr
    table_ch['p-sym'] = p_symbol
    
    table_ch.to_csv(f'{OUTDIR_TEST}/posthoc_{iv}.csv', float_format = '%.3f')
